import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SELECTEDREGION_SET:
            return setSelectedRegion(state, action)
        default:
            return state
    }
}

function setSelectedRegion(state, action) {
    const { selectedregion } = action
    return selectedregion
}