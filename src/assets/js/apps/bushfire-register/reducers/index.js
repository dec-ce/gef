import {combineReducers} from "redux"
import schools from "./schools"
import searchinput from "./searchinput"
import bushfireprone from "./bushfireprone"
import selectedschools from "./selectedschools"
import regions from "./regions"
import selectedregion from "./selectedregion"
import regionschools from "./regionschools"

export default combineReducers({
    regions,
    regionschools,
    schools,
    searchinput,
    bushfireprone,
    selectedschools,
    selectedregion
})