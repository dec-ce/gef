# GEF | Global Experience Framework
> A framework developed by the NSW Department of Education. Powered by UIkit, gulp, Babel, SASS, and pug.

Maintainers: Alex Motyka, [Alex Dewez-Lopez](https://github.com/dewezlopez)

## Getting Started

[Detailed information on getting started](https://drive.google.com/open?id=1cRtBJQwV9qwPoD5-J2E7Z1u_apvlnoY4w24hs7iGa9A)

### Proxy settings

If you're working at DoE, you'll need these proxy settings:

- http: http://proxy.det.nsw.edu.au:80
- https: http://proxy.det.nsw.edu.au:80

### Install

- Get the repo: `$ git clone https://bitbucket.org/dec-ce/gef.git`
- Install packages: `$ npm install`
- Install componentes: `$ bower install`

### Commands

- `$ gulp watch`  - compiles the src dir and serves the framework using live-reload
- `$ gulp build`  - simply builds the src dir into the dist dir
- `$ gulp deploy` - builds and sends to our test environments (employees of DoE only, requires a flag - see below)

### Flags

- `$ gulp [task] --compress`    - compresses built assets
- `$ gulp [task] --cms=matrix`  - restructures the images dir and matches CSS requirements to Matrix's asset tree
- `$ gulp deploy --env=string`  - deploy to an environment of your choosing (employees of DoE only)