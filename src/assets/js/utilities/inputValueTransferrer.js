"use strict"
import * as $ from "jquery"

/**
 * Passes values from one or multiple inputs to another after interaction
 *
 * @since 0.2.02
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2016 State Government of NSW
 *
 * @class
 * @requires jQuery
 */
class InputValueTransferrer {

  constructor(selector, config) {

    /**
     *
     * @param {string} config.selector: the element which has a value to transfer
     *
     */
    this.config = {
      selector: "[data-gef-input-transferrer]"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Check if selector has been passed to constructor
    if (selector) {
      // Use jQuery to match find the relevant DOM element(s)
      this.$inputs = $(selector)
      // Set the receiver element
      this.$receiver = $("#" + this.$inputs.eq(0).attr(this.config.selector.replace(/\[|\]/g, "")))

      this.setChangeEvents(this.$inputs, this.$receiver)
    }
  }


  /**
   * Sets the events needed to capture input changes
   *
   * @setChangeEvents
   *
   * @param {jQuery} $i - A jQuery object with relevant input elements
   * @param {jQuery} $r - A jQuery object with relevant receiver element
   *
   */
  setChangeEvents($i, $r) {
    var _this = this;

    $i.on("change", function() {
      _this.setReceiverValue($r, $(this).val())
    })
  }

  /**
   * Sets the receiver values based on the changed input
   *
   * @setReceiverValue
   *
   * @param {jQuery} $r - A jQuery object with relevant receiver element
   * @param {String} v - The value to set on the receiver
   *
   */
  setReceiverValue($r, v) {
    $r.val(v)
  }
}

export default InputValueTransferrer