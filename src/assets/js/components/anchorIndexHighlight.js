"use strict"
import * as $ from "jquery"
import ScrollToggle from "utilities/scrollToggle"

/**
* anchorIndexHighlight.js - Controls the index link highlighting and scroll tracking behaviour

* @class
* @requires jQuery
* @requires ScrollToggle class
*/

class AnchorIndexHighlight {

  /**
  * Creates a new AnchorIndexHighlight
  *
  * @constructor
  *
  * @param {String}   selector - the jQuery selector in question
  * @param {Object}   config - class configuration options. Options vary depending on need. Used for both AnchorIndexHighlight and ScrollToggle
  * @param {String}   [config.index_container = ".gef-anchor-index"] - the class the anchor index links reside in
  * @param {String}   [config.subject] - selector for the element accepts the toggle. Used for both AnchorIndexHighlight and ScrollToggle
  * @param {String}   [config.scroller_position] - if the config.scroll_window is true, check the checkpoint against the bottom, middle, top or percentage of screen. Options: "bottom"| "middle"|"top"|"percent", default: "bottom". Used for ScrollToggle
  * @param {String}   [config.scroller_percent] - the percent of the viewport which is set to pass the checkpoint. Used for ScrollToggle
  * @param {Boolean}  [config.linked] - Set to true if the checkpoint is the href value of the subject anchor tag. Used for ScrollToggle
  * @param {Boolean}  [config.scroll_window] - tests the scroll position of the window. Used for ScrollToggle
  * @param {String}   [config.toggle_class] - Set a class to toggle on and off depending if the checkpoint has passed. Used for both AnchorIndexHighlight and ScrollToggle
  * @param {Int}      [config.mobileWidth]   - Maximum width of window to make changes
  */

  constructor(selector) {

    this.config = {
        "index_container" : ".gef-anchor-index",
        "subject"         : ".gef-anchor-index li a, .gef-anchor-index option:enabled",
        "scroller_position" : "percent",
        "scroller_percent" : 30,
        "linked"          : true,
        "scroll_window"   : true,
        "toggle_class"    : "active",
        "mobileWidth": "767px"
    }

    // Set class variable(s)
    this.$mql = window.matchMedia(`(max-width: ${this.config.mobileWidth})`)

    // Instantiate the ScrollToggle
    this.scroll = new ScrollToggle(selector, this.config)

    // Set toggle_class on anchor links on click
    var component = this
    $(this.config.subject).click(function(){
      component.removeActiveState($(this))
      component.addActiveState($(this))

      // focus on content after anchor hits target
      $($(this).attr('href')).focus().click()
    })

    // Instantiate index scroll tracking as user scrolls down the page
    this.scrollTracking()
  }

  /**
  * scrolls the anchor index based on where the user is on the page
  * this is to keep the active anchor in view
  *
  * @scrollTracking
  *
  */
  scrollTracking() {
    var component = this,
        $window = $(window),
        $indexContainer = $(this.config.index_container),
        use_previous_scroll = false,
        previous_scroll_amount,
        scroll_amount

    // Allows index to return to previous scroll point if user manual scrolls index
    $indexContainer.scroll( function() {
      use_previous_scroll = true
    })

    $window.scroll( function() {
      if (!component.$mql.matches) {
        var $activeLink = $(component.config.subject).filter('.active'),
            absolute_link_position = $activeLink.length ? $activeLink.offset().top : 0

        // if absolute_link_position is default value of zero exit - no need to scroll
        if (absolute_link_position === 0) { return }

        // scroll down index container by window height if link is further down and not visible
        if (absolute_link_position > $indexContainer.offset().top + $window.height()) {
          scroll_amount = $indexContainer.scrollTop() + $window.height()
        }

        // scroll up index container by window height if link is further up and not visible
        if (absolute_link_position < $window.scrollTop()) {
          scroll_amount = $indexContainer.scrollTop() - $window.height()
        }

        // scroll the index container if scroll_amount value is provided
        if (scroll_amount) {
          $indexContainer.animate({
            scrollTop: use_previous_scroll ? previous_scroll_amount : scroll_amount
          },10)
        }

        previous_scroll_amount = scroll_amount
        use_previous_scroll = false
      }
    })
  }

  addActiveState($target) {
    $target.addClass(this.config.toggle_class)
  }

  removeActiveState($target) {
    $(this.config.subject).removeClass(this.config.toggle_class)
  }
}

export default AnchorIndexHighlight