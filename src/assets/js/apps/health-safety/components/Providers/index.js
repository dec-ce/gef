//import React from "react";
import { connect } from "reactRedux"
import Providers from "apps/health-safety/components/Providers/presenter"
import lifecycle from "reactPureLifecycle"
import { getProviders } from "apps/health-safety/actions/providers"
import { setSelectedProviders } from "apps/health-safety/actions/selectedproviders"

function mapStateToProps(state) {
    const providers = state.providers;
    const selectedschool = state.selectedschool;
    const networkcode = state.networkcode;
    const selectedproviders = state.selectedproviders;
    return {
        providers,
        selectedschool,
        networkcode,
        selectedproviders
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetProviders: () => dispatch(getProviders()),
        onGetSelectedProviders: (networkcode, providers) => setSelectedProviders(dispatch, networkcode, providers)
    }
}

const componentWillMount = (store, networkcode) => {
    store.onGetProviders();
    store.onGetSelectedProviders(networkcode, {data: {id: -1}});
};

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(Providers);

export default connect(mapStateToProps, mapDispatchToProps)(cwm);
