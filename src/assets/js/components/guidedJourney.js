"use strict"
import * as $ from "jquery"
import KeyCodes from "keycodes"
import ScrollToggle from "utilities/scrollToggle"
import ShowHideKeyboard from "components/showHideKeyboard"
import NotificationCentre from "core/notificationCentre"

/**
* guidedJourney.js - toggles the page content displayed based on the side navigation item selected

* @class
* @requires jQuery
*/

class GuidedJourney {

  /**
  * @constructor
  *
  * @param {String}   selector - the jQuery selector where we inject content
  * @param {Object}   [config.$container = $(selector)] - the object the content gets injected into
  * @param {String}   [config.selectors.side_menu = ".gef-GJ-index"] - selector for the Guided Journey menu
  * @param {String}   [config.selectors.title = ".gef-GJ-title"] - selector for the Guided Journey title
  * @param {String}   [config.toggle_class = "active"] - class used to trigger the menu active state
  * @param {int}      [config.mobileWidth]   - Maximum width of window to make changes
  */
  constructor(selector, config) {

    this.config = {
      $container : $(selector),
      selectors   : {
        side_menu : ".gef-GJ-index",
        title : ".gef-GJ-title"
      },
      toggle_class : "active",
      mobileWidth: "767px"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Save references for later
    this.$container = this.config.$container
    this.$side_menu = $(this.config.selectors.side_menu)
    this.$title = $(this.config.selectors.title)
    this.$mql = window.matchMedia(`(max-width: ${this.config.mobileWidth})`)

    // Get a reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    // Save references for ajax request and loading view
    const guided_journey_endpoint = guided_journey_endpoint_matrix ? guided_journey_endpoint_matrix : null,
          loading_message = loading_message_matrix ? loading_message_matrix : "<p>Please wait while page is being generated</p>",
          error_message = error_message_matrix ? error_message_matrix : "<p>There has been a problem , please log a ticket in Zendesk</p>"

    // Init view - prevent landing page from showing on child pages
    this.$container.removeClass("uk-hidden")
    if (window.location.hash.length > 0) {
      this.$container.empty().html(loading_message)
    }

    // Fetch data and render view
    if (guided_journey_endpoint) {
      $.ajax({
        url: guided_journey_endpoint
      }).done((data) => {
        this.data = data
        this.initJourneyMenu()
        var cachedWidth = $(window).width();
        $(window).resize(() => {
          var newWidth = $(window).width();
          //adding a check to avoid resize during ios scroll
          if (newWidth !== cachedWidth) {
            this.initJourneyMenu()
          }
        }) 

      }).fail(() => {

        if (window.location.hash.length > 0) {
          this.$container.empty().html(error_message)
        } else {
          this.toggleActiveState()
          this.$container.find(".gef-pagination").removeClass("uk-hidden")
        }

        this.$side_menu.find("[data-gef-asset-id] [href]").each( (index, link) => {
          const href = $(link).attr("href")
          $(link).attr("href", href.replace("#", "./"))
        })
      })
    } else {
      this.data = JSON.parse(`
      [{
          "hash": "",
          "asset_id": "0",
          "content": "<p class='lead'>This guideline can be used by navigating through any part via the menu or by using the previous and next buttons at the bottom of the page.<\/p><h2>Introduction<\/h2><h3>What is this?<\/h3><p>This is a policy guideline that requires schools to develop protection, prevention, early intervention and response strategies for student bullying in NSW Government Schools.<\/p><h3>Who is it for?<\/h3><p>Teachers wanting to create, manage, and implement anti- bullying measures in the school and school community.<\/p><p>Parents who wish to understand the strategic approach.<\/p><h3>How to use it?<\/h3><p>Collaborate with your senior staff members to formulate a plan using this online guideline.<\/p><h4>Download PDF<\/h4><p><a href='#'>Anti-bullying guidelines.pdf<\/a><\/p>"
      },{
          "hash": "subpage-2",
          "asset_id": "2",
          "content": "<h2>Form a school team<\/h2><p>The role of the school team is to facilitate the development and revision process. The team should be led by the principal or their nominee, who remains responsible for the final decisions relating to the school Anti- bullying Plan.<\/p><p>It is important to have representatives from across the schools, including teachers, executive, students and parents, as well as other community members. It is also important to ensure representation for those students who have special needs or who are at risk due to their cultural, ethnic or socioeconomic background.<\/p><h3>Focus questions<\/h3><p>Who are the key stakeholders in our school community?<\/p><p>* How will you ensure representation from the whole school<\/p><p>community?<\/p><p>* What expertise is required to assist the team to successfully complete its task?<\/p><p>* How should the team members be selected?<\/p><p>Groups to consider include students; staff; parents; and community members (eg community organisations that work with the school, and the cultural groups that make up the school community).<\/p>"
      },{
          "hash": "subpage-1",
          "asset_id": "1",
          "content": "<h2>Engage the school community<\/h2><p>School communities need to work together to recognise, challenge and change inequalities that fuel bullying and harassment within the school and to build trust and respect between all groups.<\/p><p>By doing this, we are better placed to meet the needs of all students and contribute to a socially just environment.<\/p><p>The school team should undertake ongoing and wide consultation with the school community. The team should present the information it has gathered, and the plan it is developing, to the school community for consideration at various stages throughout the process.<\/p><h3>Focus questions<\/h3><p>* How will you achieve a whole school approach that engages all sectors of the school community?<\/p><p>* At what stages throughout the process will the school community be consulted?<\/p><p>* What data will be presented? (Consider sensitivity and privacy issues related to some data).<\/p><p>* What questions should be asked to facilitate understanding and generate ideas? (Especially important for developing shared understanding of some issues including protection and prevention, online bullying, power relationships, bystander issues and response to bullying issues).<\/p><p>* How will perceived concerns be addressed?<\/p>"
      },{
          "hash": "subpage-4",
          "asset_id": "4",
          "content": "<h2>Develop a &lsquo;statement of purpose&rsquo;<\/h2><p>A 'statement of purpose' should be included at the beginning of the school's Anti-bullying Plan. This statement outlines the aims of the school and the key beliefs or principles upon which the plan is based.<\/p><p>Preventing and responding to bullying behaviour in learning environments is a shared responsibility between all staff, students, parents\/caregivers and members of the wider school community.<\/p><p>All members of the school community contribute to the prevention of bullying by modelling appropriate behaviour and respectful relationships.<\/p><h3>Focus questions<\/h3><p>* What principles should underpin the school's antibullying practices?<\/p><p>* What outcomes does the community want the Antibullying Plan to<\/p><p>achieve?<\/p><p>* Do all the policies, programs and practices within the school work together to achieve these outcomes?<\/p>"
      },{
          "hash": "subpage-3",
          "asset_id": "3",
          "content": "<h2>Develop a shared understanding<\/h2><p>When undertaken reflectively and collaboratively, this process can build the trust and respect needed for individuals and groups to safely challenge and change inequalities within the school. The school can then address the needs of all students and foster an inclusive school culture. Development of a working group with a diverse representation from the school community can facilitate a shared understanding of what bullying is. To succeed in creating a shared understanding it may be helpful to:<\/p><p>BUILD KNOWLEDGE AND UNDERSTANDING<\/p><p>GATHER AND ANALYSE INFORMATION<\/p><h3>Focus questions<\/h3><p>* Are all aspects of the current school Anti-bullying Plan consistent with the Bullying: Preventing and Responding to Student Bullying Policy and other relevant Departmental policies and plans.<\/p><p>* Which school trend data should be considered?<\/p>"
      },{
          "hash": "subpage-5",
          "title": "Develop or revise the school Anti-bullying Plan",
          "asset_id": "5",
          "content": "<h2>Develop or revise the school Anti-bullying Plan<\/h2><p>Include protection, prevention, early intervention and response strategies for student bullying.<\/p><p>Identify the series of strategies that will comprise your new or revised Anti- bullying Plan. Write up a plan to reflect the strategies that the team has identified.<\/p><p>Your plan should be in a format that clearly identifies, for example, your expected outcomes, targets, strategies and timeframe. Expand the fine detail of each strategy in your plan.<\/p><p>Discuss the plan and each of the individual strategies with those people who will be involved in its implementation.<\/p><h3>Focus questions<\/h3><p>* Do the strategies support and reflect the aims and beliefs articulated in the &lsquo;statement of purpose&rsquo;?<\/p><p>* Are there strategies for each of the areas for action required?<\/p><p>* Does the plan include processes to evaluate and review each strategy<\/p><p>as well as the plan as a whole?<\/p>"
      },{
          "hash": "subpage-6",
          "title": "Publication and promotion",
          "asset_id": "6",
            "content": "<h2>Publication and promotion</h2><p>Your school may find the Antibullying Plan Template useful for publishing your plan for your school community.</p><h3>Focus questions</h3><p>* How will you publish and promote the new Plan within the school community?</p><p>* Into which community languages does the school&rsquo;s Anti-bullying Plan need to be translated?</p><h3>Example Show Hide</h3><div id='showhide_auto'><div class='gef-show-hide uk-accordion' data-uk-accordion='{collapse:false, showfirst:false}' role='tablist'><h3 class='uk-accordion-title' id='at229' aria-controls='ac229' aria-expanded='false' role='tab' tabindex='0'><span> Nulla non <i></i><i class='vertical' aria-hidden='true'></i></span></h3><div class='uk-accordion-content'><p>Vivamus luctus egestas leo. Maecenas sollicitudin nullam rhoncus aliquam metus. Etiam egestas wisi a erat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor, Mauris pretium quam et urna. Fusce nibh, curabitur sagittis hendrerit ante. Aliquam erat volutpat. Vestibulum erat nulla, ullamcorper nec rutrum nonummy ac erat. </p></div><h3 class='uk-accordion-title' id='at438' aria-controls='ac438' aria-expanded='false' role='tab' tabindex='0'><span>Duis condimentum augue <i></i><i class='vertical' aria-hidden='true'></i></span></h3><div class='uk-accordion-content'><p>Fusce consectetuer risus a nunc. Aliquam ornare wisi eu metus. Integer pellentesque quam vel velit. Morbi gravida libero nec velit. Morbi scelerisque luctus velit. Etiam dui sem, fermentum vitae sagittis id malesuada in quam. Proin mattis lacinia justo. Vestibulum facilisis auctor urna. Aliquam in lorem sit amet leo accumsan lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies lacus quam ultricies odio vitae placerat pede sem sit amet enim. </p></div></div></div>"
      },{
          "hash": "subpage-7",
          "title": "Resources",
          "asset_id": "7",
          "content": "<h2>Resources</h2><p>Further reading and supporting documents on anti-bullying.</p><h3>Websites:</h3><p>Anti-bullying website Bullying. No Way!</p><p>Kids Helpline</p><p>Office of the eSafety Commissioner</p><h3>Policies:</h3><p>Bullying: Preventing and Responding to Student Bullying in Schools Policy</p><h3>Templates and guides:</h3><p><a href='#'>Anti-bullying plan template (PDF 121.36 KB)</a></p><p><a href='#'>Anti-bullying guidelines (PDF 60.29 KB)</a></p><p><a href='#'>Anti-bullying interventions in schools - what works? (PDF 4741.34 KB)</a></p>"
      }]`)

     this.initJourneyMenu()
   
    var cachedWidth = $(window).width();
    $(window).resize(() => {
      var newWidth = $(window).width();
      if (newWidth !== cachedWidth) {
        this.initJourneyMenu()
      }
    })
  
     }
    //this.initJourneyMenu()
   
    // prevent hash change when use back to menu link
    window.addEventListener("hashchange", (event) => {
      if (event.newURL.split('#').pop().includes("back_")) {
        history.pushState(null, null, event.oldURL);
      }
    })
  }

  /**
  * Init the guided journey
  * binds the UI events, injects the content, sets active state to the menu correct menu item and injects the correct nav buttons
  *
  * @initJourneyMenu
  *
  */
  initJourneyMenu() {
    // bind desktop UI
    this.bindLinkUI()
    // bind mobile UI
    this.bindSelectUI()

    // position title
    if (this.$mql.matches) {
      if (this.$container.prev().is(".gef-GJ-title")) {
        this.$title.insertBefore(this.$side_menu)
      }
    } else {
      if (!this.$container.prev().is(".gef-GJ-title")) {
        this.$title.insertBefore(".gef-GJ-main")
      }

      // this.checkForShowHides()
    }

    const content_data = this.data.filter( page => page.hash === window.location.hash.replace("#", "") )

    if (window.location.hash.length > 0) {
      if (content_data.length > 0 && content_data[0].content) {
        this.injectContentById(content_data[0].asset_id)
        this.toggleActiveState(content_data[0].asset_id)
        this.setPagination()
      }
    } else {
      // Introduction page init
      this.$container.children().first().attr("tabindex", "0")
      this.backToMenu('')
      this.toggleActiveState()
      this.setPagination()
    }
  }

  /**
  * updates the page with the correct content user clicks on journey menu link
  *
  * @bindLinkUI
  *
  */
  bindLinkUI() {
    const $link = this.$side_menu.find("[data-gef-asset-id]")

    $link.off("click")
    $link.on("click", (evt) => {
      this.updateView($(evt.currentTarget))
    }).on("keydown", (evt) => {
      if (event.which === KeyCodes.return || event.keyCode === KeyCodes.return) {
        this.updateView($(evt.currentTarget))
      }
    })
  }
  /**
  * checks for show hides and if any, loads the show hide functionality
  **/

  checkForShowHides() {
    if ($("[data-uk-accordion]").length > 0) {
      let presentComponents = $("[data-uk-accordion]")
      let showHide = new ShowHideKeyboard()
      $.each(presentComponents, function(index, componentInstance) {
        showHide.init(componentInstance)
      })
    }
  }
  /**
  * updates the page with the correct content user clicks the button in the mobile view
  *
  * @bindLinkUI
  *
  */
  bindSelectUI() {
    const $button = this.$side_menu.find("button")

    $button.off("click")
    $button.on("click", () => {
      const $target = this.$side_menu.find("option:selected")
      window.location.hash = `#${$target.attr("data-gef-hash")}`
      this.updateView($target)
    })
  }

  /**
  * updates the page contents
  * sets the active state on the menu item
  * injects the correct nav buttons
  * shifts the focus to the content after content is updated
  *
  * @updateView
  *
  */
  updateView($target) {
    if ($target) {
      const asset_id = $target.attr("data-gef-asset-id")
      this.injectContentById(asset_id)
      this.toggleActiveState(asset_id)
      this.setPagination()
      this.shiftFocusToContent()

      // this.checkForShowHides()
    }
  }

  /**
  * updates the page contents via asset id
  *
  * @injectContentById
  *
  */
  injectContentById(id) {
    const content_data = this.data.filter( page => page.asset_id === id )

    if (content_data.length > 0 && content_data[0].content) {
      // load content
      this.$container.empty().html(content_data[0].content)
      this.$container.children().first().attr("tabindex", "0")
      this.backToMenu(content_data[0].hash)
    }

    // Let any listen subscribers know that the content has updated
    this.notificationCentre.publish("domUpdate", this.$container)
  }

  /**
  * back to menu functionality
  *
  * @backToMenu
  *
  */
  backToMenu(hash) {
    // append back to menu link
    if (this.$container.find(".gef-skiplink__link").length === 0) {
      this.$container.append(`<a class="gef-skiplink__link" href="#back_${hash}">Back to menu</a>`)
    }

    this.$container.find(".gef-skiplink__link").on("keydown", (evt) => {
      // setTimeout to resolve Firefox .focus() bug
      if (evt.which === KeyCodes.return || evt.keyCode === KeyCodes.return) {
        setTimeout( () => { $($(evt.currentTarget).attr("href")).focus() }, 100)
      }
    }).on("click", (evt) => {

      if (this.$mql.matches) {
        evt.preventDefault()
      }

      // focus back to the select box on mobile
      const $select = this.$side_menu.find("nav:visible select")
      if ($select.length) {
        $select.focus().click()
      }

    })
  }

  /**
  * sets the active state on the link/select option as necessary by asset id
  * accessibility updates as well as updating the url hash
  *
  * @toggleActiveState
  *
  */
  toggleActiveState(id) {
    const $journeyNavItem = this.$side_menu.find("[data-gef-asset-id]"),
          $target = (id) ? this.$side_menu.find(`nav:visible [data-gef-asset-id=${id}]`) : this.$side_menu.find(`nav:visible [data-gef-asset-id]`).first()

    // Toggle the active state
    $journeyNavItem.removeClass(this.config.toggle_class)
    $target.toggleClass(this.config.toggle_class)

    if (this.$side_menu.find("select").length) {
      this.$side_menu.find("select").val($target.attr("value"))
    }

    // Toggle the accesibility current aria label
    $journeyNavItem.removeAttr("aria-current")
    $target.attr("aria-current", "step")

    // Update hash
    this.updateHash($target)
  }

  /**
  * shifts focus to the content
  *
  * @shiftFocusToContent
  *
  */
  shiftFocusToContent() {
    this.$side_menu.find(`nav:visible .${this.config.toggle_class} a`).blur()
    this.$container.children().first().focus().click()
  }

  /**
  * injects the pagination buttons and determines the correct button text
  * binds the navigation event
  *
  * @setPagination
  *
  */
  setPagination() {
    const $list_item = this.$side_menu.find("nav:visible [data-gef-asset-id]"),
          $active_item = $list_item.filter(`.${this.config.toggle_class}`),
          active_index = $list_item.index($active_item)

    let previous_button_text = "Previous",
        next_button_text = "Next"

    if (active_index === 0) { previous_button_text = "" }
    // if (active_index === 1) { previous_button_text = "First" }
    // if (active_index === ($list_item.length - 2)) { next_button_text = "Last" }
    if (active_index === ($list_item.length - 1)) { next_button_text = "" }

    const html = `
      ${previous_button_text.length > 0 ? `<button class='uk-button gef-button gef-button--alt1 gef-button-x-large gef-pagination__prev' aria-label='${previous_button_text} Page'><i class='uk-icon uk-icon-angle-left'></i>${previous_button_text}</button>` :``}
      ${next_button_text.length > 0 ? `<button class='uk-button gef-button gef-button-x-large gef-pagination__next' aria-label='${next_button_text} Page'>${next_button_text}<i class='uk-icon uk-icon-angle-right'></i></button>` :``}
    `

    this.$container.find(".gef-pagination").remove()
    this.$container.append(`<div class="gef-pagination">${html}</div>`)
    this.bindPagination()
  }

  /**
  * binds pagination UI
  *
  * @bindPagination
  *
  */
  bindPagination() {
    this.$container.find(".gef-pagination .uk-button").on("click", (evt) => {
      this.paginate($(evt.currentTarget))
    }).on("keydown", (evt) => {
      if (event.which === KeyCodes.return || event.keyCode === KeyCodes.return) {
        this.paginate($(evt.currentTarget))
      }
    })
  }

  /**
  * updates the view and hash when paginate
  *
  * @paginate
  *
  */
  paginate($button) {
    const $current_item = this.$side_menu.find(`nav:visible .${this.config.toggle_class}`)
    let $target

    // update page contents
    if ($button.hasClass("gef-pagination__prev")) {
      $target = $current_item.prev()
    }
    if ($button.hasClass("gef-pagination__next")) {
      $target = $current_item.next()
    }

    this.updateView($target)
    this.updateHash($target)
  }

  /**
  * update the url hash
  *
  * @updateHash
  *
  */
  updateHash($target) {
    let hash = ''

    if ($target.attr("data-gef-hash")) {
      hash = $target.attr("data-gef-hash")
    } else {
      hash = $target.attr("href") ? $target.attr("href") : $target.find("[href]").attr("href")
    }

    if (hash) {
      window.location.hash = hash
    }
  }
}

export default GuidedJourney
