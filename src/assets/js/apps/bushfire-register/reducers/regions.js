import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.REGIONS_SET:
            return setRegions(state, action)
        default:
            return state
    }
}

function setRegions(state, action) {
    const { regions } = action
    return regions
}