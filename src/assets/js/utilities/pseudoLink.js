"use strict"
import * as $ from "jquery"
import NotificationCentre from "core/notificationCentre"

/**
 * Create a new PseudoLink
 *
 * @example
 * // Instantiate and check/tag the passed link using class constructor
 * let pseudoLink = new PseudoLink("[data-pseudo-link]", { selectors: { container: "[data-pseudo-link]" }})
 *
 * @class
 * @requires jQuery
 */
class PseudoLink {

  /**
   * @param {(String|jQuery)} selector - A CSS selector or jQuery object
   * @param {string} config.classes.container - a class which is added to the selector element
   *
   * @returns {LinkTagger} Returns a PseudoLink object
   */
  constructor(selector, config) {

    this.config = {
      classes: {
        container: "gef-pseudo-link",
      },
      selectors: {
        anchors: "a"
      }
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      $.extend(this.config, config)
    }

    // Check if selector has been passed to constructor
    if (selector) {
      let $container = $(selector)
      let localConfig = $container.attr("data-pseudo-link")

      if (localConfig != "data-pseudo-link" && localConfig != false) {
        this.config.selectors.anchors = localConfig
      }

      // Use jQuery to find the relevant selector in the DOM
      this.addLinks(selector)

      
    }

    let notificationCentre = NotificationCentre.shared()
    notificationCentre.publish("pseudoLinkInit")
  }


  /**
   *
   *
   * @param {Sring|jQuery|Element} selector - A CSS selector or DOM element to turn into a "link"
   *
   * @returns {jQuery|false} Returns the result of a jQuery match however if zero elements are match then false will be returned
   */
  addLinks(selector) {
    let $containers = $(selector)
    let _self = this

    if ($containers.length !== 0) {
      $containers.each(function(index, container) {
        _self.addLink(container)
      })

      return $containers
    } else {
      return false
    }
  }


  /**
   * Turns a container element (e.g. div) that contains an anchor (immediate child) into clickable element that links to the same url as it's child anchor
   *
   * @param {Element} container - A DOM element containing an anchor tag
   *
   * @returns jQuery The element encapsulated in a jQuery object
   */
  addLink(container) {
    let $container = $(container)
    let anchor = $container.find(this.config.selectors.anchors)

    // Check if there is 1 or more anchors in the container
    if (anchor.length > 0) {
      // Grab the first anchors
      let $anchor = $(anchor[0])
      let url = $anchor.attr("href")
      let target = $anchor.attr("target")

      $container.addClass(this.config.classes.container)

      $container.on("click", function() {
        window.open(url, (target ? target : "_self"))
      })
    }

    return $container
  }

}

export default PseudoLink
