import React from "react"
import ReactAutocomplete from "reactAutocomplete"

const SearchForm = (props) => {
    const schools = props.schools
    const searchinput = props.searchinput
    const bushfireprone = props.bushfireprone
    const onSetSearchInput = props.onSetSearchInput
    const onSetSelectedSchools = props.onSetSelectedSchools

    const handleSearch = (event) => {
        event.preventDefault()
        onSetSelectedSchools(searchinput, bushfireprone)
    }

    if (!schools) {
        return ""
    } else {
        return (
            <div className={"bushfire-search"}>
                <form name={"frmSchool"} id={"frmSchool"} className={"gef-search uk-form gef-search--body"} onSubmit={handleSearch}>
                    <fieldset>
                        <legend className={"show-on-sr"}>Search the departments bushfire register</legend>
                        <label htmlFor={"school_search_input"}>
                            <span className={"show-on-sr"}>Search the departments bushfire register</span>
                        </label>

                        <ReactAutocomplete
                            inputProps={{
                                id: "school_search_input",
                                size: 50,
                                "aria-haspopup": true,
                                maxLength: 254,
                                type: "text",
                                name: "schoolName",
                                className: "ui-autocomplete-input"
                            }}
                            items={schools}
                            shouldItemRender={(item, value) => value.length >= 3 && item.toLowerCase().indexOf(value.toLowerCase()) > -1}
                            getItemValue={item => item}
                            renderItem={(item, highlighted) =>
                                <div key={item}
                                    style={{backgroundColor: highlighted ? "#eee" : "transparent"}}>
                                    {item}
                                </div>
                            }

                            value={searchinput === null ? "" : searchinput}
                            onChange={element => onSetSearchInput(element.target.value)}
                            onSelect={value => onSetSelectedSchools(value, bushfireprone)}
                        />
                        <button type={"button"} id={"submitSchoolSearch"} aria-label={"Submit search"} tabIndex="0" onClick={handleSearch}>
                            SEARCH
                        </button>
                    </fieldset>
                </form>
            </div>
        )
    }
}

export default SearchForm
