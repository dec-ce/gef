"use strict"
import * as $ from "jquery"
import KeyCodes from "keycodes"
import UI from "uikit"
import "vendor/annyang/annyang"

/**
 * Creates a new HeyGEF. Inserts a language specific HeyGEF at the beginning of a matched element. The language is detected using the HTML lang attribute
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */
class HeyGEF {

  /**
   * Creates a new HeyGEF
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   * @param {Object} config.HeyGEF - A plain Object containing 1 or more key/value pairs where the language code is used as the key and the HeyGEF is the value
   * @param {String} config.language - the default language code to use when lang attribute is not present or a greetign for the lang code is not found
   *
   * @example
   * // Instantiate a new HeyGEF
   * let HeyGEF = new HeyGEF(".gef-main-content .lead", { HeyGEF: { de: "Hallo" } })
   */
  constructor(selector, config) {

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    // Default class config options
    this.config = {
      commands: {
        "search for *query": this.search.bind(this)
      },
      keyCommand: [KeyCodes.upArrow, KeyCodes.upArrow, KeyCodes.downArrow, KeyCodes.downArrow, KeyCodes.leftArrow, KeyCodes.rightArrow, KeyCodes.leftArrow, KeyCodes.rightArrow, KeyCodes.b, KeyCodes.a],
      searchURI: "http://search.det.nsw.edu.au/search?client=search_dec_nsw_gov_au_frontend&output=xml_no_dtd&proxystylesheet=search_dec_nsw_gov_au_frontend&site=public_ce_dec_nsw_gov_au&access=p&q=",
      debug: false
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    this.progress = 0

    $(window).keyup((event) => {
      if (this.checkKeyCommand(event)) {
        this.build()
        this.listen()
      }
    })


  }


  build() {
    let modalDiv = $("body").append(`<div id="heyGEF" class="uk-modal" style="z-index: 6000"><div class="uk-modal-dialog">Listening...</div></div>`)
    this.modal = UI.modal("#heyGEF")
  }


  checkKeyCommand(event) {
    // If keypress is euqal to currently expected keyCommand
    if (event.which === this.config.keyCommand[this.progress]) {
      if (this.progress === this.config.keyCommand.length - 1) {
        this.progress = 0
        return true
      }
      this.progress++
    } else {
      this.progress = 0
    }
    return false
  }

  print(userSaid, commandText, phrases) {
    this.modal.find(".uk-modal-dialog").text(userSaid)
  }

  listen() {
    if (annyang) {

      if ( !this.modal.isActive() ) {
        this.modal.show()
      }

      annyang.addCommands(this.config.commands)
      annyang.debug(this.config.debug)
      annyang.addCallback("resultMatch", this.print.bind(this))
      annyang.start()
    }
  }


  search(query) {
    window.location = `${this.config.searchURI}${query}`
  }


  /**
   * Static function to instatiate the HeyGEF class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {ExampleClass} Reference to the same ExampleClass instatiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new HeyGEF(selector, config)
  }

}


/**
 * Exports the HeyGEF class as a module
 * @module
 */
export default HeyGEF
