"use strict"

var anchorBoxSpec = function(AnchorBox) {


  describe("AnchorBox is using default settings", function() {
    var component,
        $object,
        selector = "[data-gef-anchor-box]"

    beforeEach(function() {
      loadFixtures("components/anchorBox.html")
      $object = $j(selector)
      component = new AnchorBox(selector)
    })

    it("$object will match the container", function() {
      expect($object[0]).toBe(component.$container[0])
    })

    it("Expects there to be 3 anchors", function() {
      expect($object.find("a").length).toBe(3)
    })

    it("Expects IDs to have been added", function() {
      expect($object.find("> h2").attr('id')).not.toBe(undefined)
    })
  })


  describe("AnchorBox is passed an element to inject after", function() {
    var component,
        selector = "[data-gef-anchor-box]",
        config = {
          inject_after_me: ".test-container"
        }

    beforeEach(function() {
      loadFixtures("components/anchorBox.html")
      component = new AnchorBox(selector, config)
    })

    it("Expects '.gef-anchor-box' to come after '.test-container'", function() {
      expect($j(config.inject_after_me).next()).toHaveClass("gef-anchor-box")
    })
  })

  describe("AnchorBox is passed a larger 'amount' than there are targets", function() {
    var component,
    selector = "[data-gef-anchor-box]",
    config = {
      amount: 4
    }

    beforeEach(function() {
      loadFixtures("components/anchorBox.html")
    })

    it("Expects to silently fail", function() {
      expect(function() { new AnchorBox(selector, config) }).toThrowError(RangeError)
    })
  })

  describe("AnchorBox can't find the inject_after_me element", function() {
    var component,
    selector = "[data-gef-anchor-box]",
    config = {
      inject_after_me: ".cant-find-me",
      inject_backup:   "h1:first-of-type"
    }

    beforeEach(function() {
      loadFixtures("components/anchorBox.html")
      component = new AnchorBox(selector, config)
    })

    it("Expects to be injected after the first h1", function() {
      expect($j(config.inject_backup).next()).toHaveClass("gef-anchor-box")
    })
  })
}

define(["/base/dist/assets/js/components/anchorBox.js"], anchorBoxSpec)
