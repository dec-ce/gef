import * as actionTypes from "apps/health-safety/constants/actionTypes"

const initialState = null;

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SEARCHINPUT_SET:
            return setSearchInput(state, action);
        default:
            return state;
    }
}

function setSearchInput(state, action) {
    const { searchinput } = action;
    return searchinput;
}