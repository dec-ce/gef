import {
    NETWORK_CODE_GET_FULFILLED,
    NETWORK_CODE_GET_REJECTED,
    NETWORK_CODE_GET_REQUESTED,
    SELECTEDSCHOOL_SET
} from "apps/health-safety/constants/actionTypes";
import {curry} from "lodash";
import {setSelectedProviders} from "apps/health-safety/actions/selectedproviders";
import axios from "axios";

export const setSelectedSchool = curry((dispatch, selectedschool, providers) => {
    dispatch({
        type: SELECTEDSCHOOL_SET,
        selectedschool
    });

    if (!selectedschool) return;
    setNetworkCode(dispatch, selectedschool, providers);
});

export const setNetworkCode = (dispatch, selectedschool, providers) => {
    // Now also get the network code of the school
    dispatch(getNetworkCodeRequestedAction());

    return axios.get(
        `https://doe-health-and-safety.firebaseio.com/networks.json?orderBy="name"&equalTo="${selectedschool.network}"`
    ).then(response => {
        const network = response.data;
        if (network === null) {
            dispatch(getNetworkCodeRejectedAction());
            return;
        }

        const networkcode = Object.keys(network)[0];
        dispatch(getNetworkCodeFulfilledAction(networkcode));

        setSelectedProviders(dispatch, networkcode, providers);
    }).catch((error) => {
        console.log(error);
        dispatch(getNetworkCodeRejectedAction());
    });
};

function getNetworkCodeRequestedAction() {
    return {
        type: NETWORK_CODE_GET_REQUESTED
    };
}

function getNetworkCodeFulfilledAction(networkcode) {
    return {
        type: NETWORK_CODE_GET_FULFILLED,
        networkcode
    }
}

function getNetworkCodeRejectedAction() {
    return {
        type: NETWORK_CODE_GET_REJECTED
    }
}
