import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SELECTEDSCHOOLS_SET:
            return setSelectedSchools(state, action)
        default:
            return state
    }
}

function setSelectedSchools(state, action) {
    const { selectedschools } = action
    return selectedschools
}