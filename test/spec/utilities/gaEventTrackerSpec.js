"use strict"

var gaEventTrackerSpec = function(GaEventTracker) {

  describe("gaEventTracker is loaded properly,", function() {
    var utility,
        config,
        configChangeTest = ['mp3', 'ogg', 'wav', 'wma']

    beforeEach(function() {
      config  = {
        events: {
          downloads: {
            types : {
              "Audio": configChangeTest
            }
          }
        }
      }
      utility = new GaEventTracker(config)

    })

    it("Loads the config", function() {
      expect(utility.config).not.toBe(undefined)
    })

    it("Extends the config with the extra settings", function() {
      expect(utility.config.events.downloads.types.Audio).toEqual(configChangeTest)
    })
  })

  describe("gaEventTracker has collected and binded the download types in the config to the relevant anchors,", function() {
    var utility,
        config

    beforeEach(function() {
      spyOn(GaEventTracker.prototype, 'applyDownloadEvent').and.callThrough()
      utility = new GaEventTracker(config)
    })

    it("Runs the applyDownloadEvent function", function() {
      expect(GaEventTracker.prototype.applyDownloadEvent).toHaveBeenCalled()
    })
  })

}

define(["/base/dist/assets/js/utilities/gaEventTracker.js"], gaEventTrackerSpec)