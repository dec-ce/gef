import { SELECTEDPROVIDERS_ADD, SELECTEDPROVIDERS_CLEAR } from "apps/health-safety/constants/actionTypes"
import _ from "lodash"
import axios from "axios"

export function setSelectedProviders(dispatch, networkcode, providers) {
    dispatch({
        type: SELECTEDPROVIDERS_CLEAR
    });

    _.transform(providers, (result, value, key) => {
        let trainers = 0;
        let ncode = "";
        _.forEach(value.networks, (network) => {
            if (network.code === networkcode) {
                trainers = network.trainers;
                ncode = network.code;
            }
        });

        if (trainers !== 0) {
            let combinedmin = 0, combinedmax = 0, anamin = 0, anamax = 0, cprmin = 0, cprmax = 0;

            return axios.get(
                `https://doe-health-and-safety.firebaseio.com/courses.json?orderBy="provider"&equalTo="${key}"`
            ).then(response => {
                _.forEach(response.data, (course) => {
                    if ("CPR" === course.name) {
                        cprmin = course.cost.min;
                        cprmax = course.cost.max;
                    } else if ("Anaphylaxis" === course.name) {
                        anamin = course.cost.min;
                        anamax = course.cost.max;
                    } else if ("Anaphylaxis/CPR" === course.name) {
                        combinedmin = course.cost.min;
                        combinedmax = course.cost.max;
                    }
                });

                let combined = "";
                if (!combinedmin) combined = "$" + combinedmax + "*";
                if (!combinedmax) combined = "$" + combinedmin + "*";
                if (combinedmin && combinedmax) combined = "$" + combinedmin + " - $" + combinedmax + "*";

                let anaphylaxis = "";
                if (!anamin) anaphylaxis = "$" + anamax + "*";
                if (!anamax) anaphylaxis = "$" + anamin + "*";
                if (anamin && anamax) anaphylaxis = "$" + anamin + " - $" + anamax + "*";

                let cpr = "";
                if (!cprmin) cpr = "$" + cprmax + "*";
                if (!cprmax) cpr = "$" + cprmin + "*";
                if (cprmin && cprmax) cpr = "$" + cprmin + " - $" + cprmax + "*";

                const selectedprovider = {
                    name: value.name,
                    email: value.email,
                    contact: value.contact,
                    phone: value.phone,
                    network: ncode,
                    trainers: trainers,
                    combined: combined,
                    anaphylaxis: anaphylaxis,
                    cpr: cpr
                };

                dispatch({
                    type: SELECTEDPROVIDERS_ADD,
                    selectedprovider
                })
            }).catch((error) => {
                console.log(error);
            });
        }
    });
}
