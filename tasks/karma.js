"use strict"

module.exports = function(gulp, plugins, options, errorHandler, KarmaServer) {
  return function(done) {

    // Don't run the uni tests if the compress flag is set to true
    if (options.compress) {
      new KarmaServer({
        configFile: __dirname + "/../karma.conf.js",
        singleRun: true
      }, done).start()
    }
  }
}
