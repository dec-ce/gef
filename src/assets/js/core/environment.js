"use strict"

let Environment = {
    dev: "http://localhost",
    test: "https://dec-ce.bitbucket.io",
    education: {
        prod: "https://education.nsw.gov.au",
        preProd: "https://pre.education.nsw.gov.au",
        nonProd: "https://uat.education.nsw.gov.au"
    },
    matrix: {
        prod: "https://cms.det.nsw.edu.au",
        preProd: "https://cms.pre.det.nsw.edu.au",
        nonProd: "https://cms.uat.det.nsw.edu.au"
    },
    search: {
        prod: "https://search.det.nsw.edu.au",
        preProd: "https://search.uat.det.nsw.edu.au",
        nonProd: "https://search.uat.det.nsw.edu.au"
    },
    microservices: {
        prod: "https://services.uat.education.nsw.gov.au",
        preProd: "https://services.uat.education.nsw.gov.au",
        nonProd: "https://services.uat.education.nsw.gov.au"
    },
    onisearch: {
        prod: "https://search.education.nsw.gov.au",
        preProd: "https://search.pre.education.nsw.gov.au",
        test: "https://search.education.nsw.gov.au",
        nonProd: "https://search.dev.education.nsw.gov.au",
        dev: "https://search.dev.education.nsw.gov.au"
    },

    get: (environment, system) => {

        if (environment === "auto") {
            // If non-prod Matrix (uat.education.nsw.gov.au) OR developer's computer (localhost) OR template test server (dec-ce.bitbucket.io)
            if (new RegExp("uat\.education\.nsw\.gov\.au|localhost|dec-ce\.bitbucket\.io").test(window.location.host)) {
                environment = "nonProd"
            // If pre-prod Matrix (pre.education.nsw.gov.au)
            } else if (new RegExp("pre\.education\.nsw\.gov\.au").test(window.location.host)) {
                environment = "preProd"
            // Otherwise default to prod environment
            } else {
                environment = "prod"
            }
        }
        
        return Environment[system][environment]
    }
}

/**
 * Exports the Environment class as a module
 * @module
 */
export default Environment