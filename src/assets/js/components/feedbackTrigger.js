"use strict"
import * as $ from "jquery"

/**
 * FeedbackTrigger.js Binds a custom link to the JIRA collection dialogue.
 * Documentation on JIRA issue collector here: https://confluence.atlassian.com/jira/advanced-use-of-the-jira-issue-collector-296092376.html
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class FeedbackTrigger {

  constructor(selector, config) {

    this.config = {}

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    var trigger = selector

    // Load JIRA issue collector script
    $.ajax({
      url: "https://dec-ce.atlassian.net/s/bce8b55631bc346785300216f98281d8-T/en_UKrrn36f/71001/be09033ea7858348cd8d5cdeb793189a/2.0.10/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-UK&collectorId=13389cd6",
      type: "get",
      cache: true,
      dataType: "script",
      complete: function() {
        // When the script has been loaded bind the trigger button to the collector dialogue
        window.ATL_JQ_PAGE_PROPS = $.extend(window.ATL_JQ_PAGE_PROPS, {
          triggerFunction : function( showCollectorDialog ) {
            $(trigger).on("click", function(e) {
              e.preventDefault()
              showCollectorDialog()
              //Move the element so it can be tabbed with keyboard in all browsers.
              var popup = document.getElementById('atlwdg-container')
              $('.gef-skiplink-container').before(popup)              
            })
          }
        })
      }
    })

  }

}

export default FeedbackTrigger