"use strict"
import angular from "angular"
import AngularSanitize from "angularSanitize"
import angularUtilsPaginate from "angularUtilsPaginate"
import * as $ from "jquery"
import services from "apps/search/services/services"
import controllers from "apps/search/controllers/controllers"
import filters from "apps/search/filters/filters"
import "apps/search/filters/filters"
import "apps/search/services/config"
import "apps/search/services/utils"
import "apps/search/controllers/searchFormCtrl"
import "apps/search/controllers/showResultsCtrl"
import "apps/search/filters/sanitize"
import "apps/search/services/search"
import "apps/search/aria/aria"

/**
 * main.js
 * Initialising the search application using angular.js
 * Application is single page call to GSA resulting.
 * Third party angualar codes used are:
 *  - angular :  default angular code
 *  - angularSanitize : allows display of HTML code from jsonp responses
 *  - angularUtilsPaginate : handles pagination and standard display
 */
class SearchSPA {

  constructor(selector, config) {

    this.config = {}

    $.extend(this.config, config)
    this.selector = $(selector)

    // initiates the app on looking for the an item with ID SearchSPA.
    // specifies requires services, controllers and filters.
    this.app = angular.module("SearchSPA", ["services", "controllers", "filters", "angularUtils.directives.dirPagination"])

    this.app.config(["$locationProvider", function($locationProvider) {
      // this is required for the location to be able to find queryString parameters.
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      })
    }])

    angular.bootstrap(this.selector, ["SearchSPA"])
  }

  static shared(selector, config) {
    this.instance != null ? this.instance : this.instance = new SearchSPA(selector, config)
    return this.instance
  }

}

export default SearchSPA
