"use strict"
import User from "core/user"
import * as $ from "jquery"


/**
 * The Historian module displays the user's last 10 viewed pages
 *
 * @since 1.8.0
 *
 * @author Alex Motyka <alex.motyka@det.nsw.edu.au>, 
 * @copyright © 2019 State Government of NSW 2016
 *
 * @class
 */
class Historian {

  /**
   * Constructor for Historian
   *
   * @constructor
   *
   * @param {String|Element|jQuery} [selector] - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options
   *
   * @example
   * // Instantiate a new Historian as a singleton
   * let Historian = Historian.shared()
   */
  constructor(selector, config) {

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    this.config = {
      render: false
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    if (selector) {
      this.$container = $(selector)
    }

    // Get reference to User singleton
    this.user = User.shared()

    // Only render the User's browsing history in the UI if config.render = true
    if (this.config.render) {
      this.$container.addClass("gef-link-columns")
      this.$container.html(this.getAsHTML())
    }
    
  }


  /**
   * Returns a GEF Link Column <LI> item encapsulated in a jQuery object 
   * 
   * @param {*} pageTitle Page title
   * @param {*} pageURL  Page URL
   * @param {*} divisionTitle Division title
   * @param {*} divisionURL Division URL
   * 
   * @returns {jQuery} GEF Link Column LI item HTML encapsulated in a jQuery object 
   */
  creatLinkColumnItem(pageTitle, pageURL, divisionTitle, divisionURL) {
    return $(`<li><a href="${pageURL}">${pageTitle}</a><br /><a href="${divisionURL}" class="gef-link-list__sub-link">${divisionTitle}</a></li>`)
  }


  /**
   * Return User's browsing history (last 10 pages) as a GEF Link Column HTML component
   * 
   * @returns {jQuery} GEF Link Column HTML component encapsulated in a jQuery object 
   */
  getAsHTML() {

    let linkColumnsList = $(`<ul class="gef-link-list gef-link-columns__items gef-link-columns--2col"></ul>`)

    for (let item of this.user.history) {
      let linkColumnsListItem = this.creatLinkColumnItem(item.title, item.url, item.division.title, item.division.url)
      linkColumnsList.append(linkColumnsListItem)
    }

    return linkColumnsList
  }


  /**
   * Static function to instantiate the Historian class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {Historian} Reference to the same Historian instantiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new Historian(selector, config)
  }

}

/**
 * Exports the tabpanelPersonalisation class as a JS module
 * @module
 */
export default Historian