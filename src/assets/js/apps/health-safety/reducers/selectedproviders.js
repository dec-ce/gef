import * as actionTypes from "apps/health-safety/constants/actionTypes"

const initialState = null;

export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.SELECTEDPROVIDERS_CLEAR:
            return clearSelectedProviders();
        case actionTypes.SELECTEDPROVIDERS_ADD:
            return addSelectedProvider(state, action);
        default:
            return state;
    }
}

function clearSelectedProviders() {
    return [];
}

function addSelectedProvider(state, action) {
    const {selectedprovider} = action;
    if (!selectedprovider) {
        return state;
    } else {
        return [...state, selectedprovider];
    }
}