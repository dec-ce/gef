"use strict"
import * as $ from "jquery"
import AnchorFilter from "utilities/anchorFilter"
import ScrollToggle from "utilities/scrollToggle"

class AnchorHighlight {

  constructor(selector) {

    this.scroll_config = {
        "subject"         : "[data-character-name] a",
        "scroller_position" : "percent",
        "scroller_percent" : 5,
        "linked"          : true,
        "scroll_window"   : true,
        "toggle_class"    : "active"
      }

    // Instantiate the AnchorFilter
    this.anchor = new AnchorFilter(selector)

    //Remove unneeded attributes
    $('span').parent().removeAttr('data-character-name')

    // Instantiate the ScrollToggle
    this.scroll = new ScrollToggle(selector, this.scroll_config)

    //IE fix
    if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)){
      $("li a[href*='#Q']").each(function(){
        $(this).attr('href','#Qu')
      });
      $("li span[id*='Q']").each(function(){
        $(this).attr('id','Qu')
      });
    }

    // Highlight bottom letters that wont be triggered by scrollToggle
    $('.gef-a-z-anchors li a').click(function(){
      var _this = $(this)
        window.onscroll = function(ev) {
            if(_this != null){
              $('.gef-a-z-anchors li a').removeClass('active')
              _this.addClass('active')
              _this = null;
            }
          else{
              _this = null;
          }
        }
      if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight){
        if(_this != null){
              $('.gef-a-z-anchors li a').removeClass('active')
              _this.addClass('active')
        }
        else{
          _this = null;
        }
      }
    });
  }
}

export default AnchorHighlight