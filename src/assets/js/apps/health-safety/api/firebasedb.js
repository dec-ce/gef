import firebase from "firebase";

// Initialize Firebase
const config = {
    apiKey: "AIzaSyCgaHcrd8CrMH9C1Aa8MejxGKLqXe0BMSw",
    authDomain: "doe-health-and-safety.firebaseapp.com",
    databaseURL: "https://doe-health-and-safety.firebaseio.com",
    projectId: "doe-health-and-safety",
    storageBucket: "doe-health-and-safety.appspot.com",
    messagingSenderId: "355216506926"
};

firebase.initializeApp(config);
export default firebase.database();