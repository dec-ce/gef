import React from "react"

const SearchRegionForm = (props) => {
    const regions = props.regions
    const selectedregion = props.selectedregion
    const bushfireprone = props.bushfireprone
    const onGetBushfireProneRegion = props.onGetBushfireProneRegion
    const onSetSelectedRegion = props.onSetSelectedRegion

    const handleSelect = (event) => {
        onSetSelectedRegion(event.target.value)
    }

    const handleClick = (event) => {
        event.preventDefault()
        onGetBushfireProneRegion(selectedregion, bushfireprone)
    }

    if (regions) {
        return (
            <div className={"bushfire-search"}>
                <form name={"frmRegion"} id={"frmRegion"}
                      className={"gef-form gef-form--inline uk-width-large-8-10 uk-margin-remove"}
                      onSubmit={handleClick}>
                    <fieldset>
                        <legend className={"show-on-sr"}>Bush fire area list</legend>
                        <label id={"region_name_label"} htmlFor={"region_name"} className={"show-on-sr"}>Search NSW fire
                            area</label>
                        <select name={"regionName"} id={"region_name"} value={selectedregion} onChange={handleSelect}>
                            <option value={"default"}>Please select a NSW fire area</option>
                            {
                                regions.map((region, key) => {
                                    return (
                                        <option key={key} value={region}>{region}</option>
                                    )
                                })
                            }
                        </select>
                        <button type={"button"} id={"submit_school_firearea"} aria-label={"Submit search"}
                                tabIndex={"0"} onClick={handleClick}>
                            SEARCH
                        </button>
                    </fieldset>
                </form>
            </div>
        )
    } else {
        return ""
    }
}

export default SearchRegionForm
