import {SELECTEDREGION_SET} from "apps/bushfire-register/constants/actionTypes"

export const setSelectedRegion = (selectedregion) => {
    return dispatch => {
        dispatch({
            type: SELECTEDREGION_SET,
            selectedregion
        })
    }
}