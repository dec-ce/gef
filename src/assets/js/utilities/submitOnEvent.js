"use strict"
import * as $ from "jquery"
import KeyCodes from "keycodes"
/**
* submitOnEvent.js - Submits a form when an event that is passed in is triggered
*
* @since 0.4.4
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class SubmitOnEvent {
  /**
  * Creates a new SubmitOnEvent Scenario
  *
  * @constructor
  *
  * @param {String} selector - the jQuery selector in question
  * @param {Object} config - config array, expected to contain triggers and submitForm
  * Expected params of triggers and submitForm have defaults set in the config
  * @name {Jquery} config.$selector - the element which the event is triggered on
  * @name {String} config.triggers - the events which trigger the submission
  * @name {Object} config.keys - the keyboard keys to watch for when keystoke related events are used @example {"keypress": "return", "keyup": "space"}
  * @name {String} config.submitForm - the form which will be submitted when the event is triggered
  * @name {Boolean} config.test - set to true only for unit testing - avoids actually submitting a form which breaks the test.
  */

  constructor(selector, config)  {

    this.config = {
      $selector    : $(selector),
      triggers     : "click",
      keys         : undefined,
      submitForm   : "form:first",
      test         : false
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    this.$form = $(this.config.submitForm)
    this.setListener(this.config.triggers, this.$form, this.config.keys)

  }

  /**
   *  If form passed through, submits that form
   *  @param {Object} form - gets submitted
   **/

  submitForm(form) {
    let $form = $(form)
    if ($form.length && !this.config.test) {
      $form.submit()
    } else if (this.config.test) {
      // console.warn("Test form submitted")
    } else {
      console.warn("No form found on page")
    }
  }


  /**
  * Sets listeners on the selector, based on the triggers sent in.
  * @param {String} triggers - the events which trigger the submission
  * @param {Jquery} form - Jquery object for the form element which gets submitted
  * @param {Object} keys - optional the keys and assocaited keystrokes which get triggered
  *
  **/
  setListener(triggers, form, keys) {
    let _self = this
    // check for events
    this.config.$selector.on(triggers, function(event) {
      event.preventDefault()

      // if keys is set and is a string then test keystroke event
      if (keys !== undefined && typeof keys === "object") {
        var key = _self.findKeystrokeEvents(triggers, keys)

        // Test the keystroke event against the key
        if (event.which === KeyCodes[key]) {
          _self.submitForm(form)
        }
      } else {
        _self.submitForm(form)
      }

    })
  }

  /**
  * Finds and matches keystroke events with keys
  * @param {String} triggers - events to test against keystroke events
  * @param {Object} keys - keystroke events and key sets to match with triggers
  * @return {Object} - the matching key
  */
  findKeystrokeEvents(triggers, keys) {
    var match = triggers.match(/keypress|keydown|keyup/g)

    if (match) {
      return keys[match[0]]
    }
  }
}


/**
 * SubminOnEvent button manager module
 * @module components/SubmitOnEvent
 */
export default SubmitOnEvent
