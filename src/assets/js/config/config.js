"use strict"

/*--- Cookies ---*/
import CookieControl from "utilities/cookieControl"

// NoticeRibbonCookie
var noticeCookie        = new CookieControl(null),
    noticeCookieName    = "gefNoticeRibbonCookie",
    noticeCookieTimeout = 60*60*24*30*6, // 6 months or 15552000 seconds
    noticeCookieValue   = "true", // the value to set on the cookie
    noticeOpenByDefault = true,
    noticeCookieExists  = noticeCookie.returnCookie(noticeCookieName)

console.log(noticeCookie.getCookies())

if (noticeCookieExists == noticeCookieValue) {
  noticeOpenByDefault = false
} else {
  noticeCookie.setCookie(noticeCookieName, noticeCookieValue, noticeCookieTimeout)
}



var configuration = {
  "name" : "GEF",
  "components" : {

    "diagnosticHUD" : {
      "selector"  : "body",
      "script"    : "components/diagnosticHUD",
      "singleton" : true,
      "test"      : function() {
        if (window.location.href.match("debug=true")) {
          return true
        }
        return false
      }
    },

    "user" : {
      "selector"  : "body",
      "script"    : "core/user",
      "singleton" : true
    },

    "socialMedia" : {
      "selector" : "[data-gef-social-media]",
      "script"   : "components/socialMedia"
    },

    "liveFyre" : {
      "selector" : ".livefyre-mosaic",
      "script"   : "components/livefyre"
    },

    "switcher" : {
      "selector" : "[data-gef-uk-switcher]",
      "script"   : "utilities/uiKitStrapper",
      "config"   : {
        "ukComponent" : "switcher",
        "optionsAttr" : "data-gef-uk-switcher"
      }
    },

    "datePicker" : {
      "selector" : ".gef-date-picker",
      "script"   : "components/datepicker",
    },

    "margin" : {
      "selector" : "[data-gef-uk-margin]",
      "script"   : "utilities/uiKitStrapper",
      "config"   : {
        "ukComponent" : "stackMargin"
      }
    },

    "smoothScroll" : {
      "selector" : "[data-gef-uk-smooth-scroll]",
      "script"   : "utilities/uiKitStrapper",
      "config"   : {
        "ukComponent" : "smoothScroll",
        "optionsAttr" : "data-gef-uk-smooth-scroll"
      }
    },

    "pseudoLink" : {
      "selector" : "[data-pseudo-link]",
      "script"   : "utilities/pseudoLink"
    },

    "breadcrumbsEnhanced" : {
      "selector" : ".gef-breadcrumbs.enhanced",
      "script"   : "components/breadcrumbsEnhanced"
    },

    "historian" : {
      "selector" : "#gef-user-history",
      "script"   : "components/historian",
      "config"   : {
        "render" : true
      }
    },

    "showHideKeyboard" : {
      "selector" : "[data-uk-accordion]",
      "script"   : "components/showHideKeyboard"
    },

    "mobileNav" : {
      "selector"  : ".gef-search__toggle",
      "script"    : "components/mobileNav",
      "singleton" : true
    },

    "formValidator" : {
      "selector"  : ".gef-form-validate",
      "script"    : "utilities/formValidator",
      "singleton" : false
    },

    "IE9Placeholder" : {
      "selector" : ".gef-ie9",
      "script"   : "components/ie9-placeholder"
    },

    "externalLink" : {
      "selector" : ".gef-main *:not('.gef-add-this') a, [data-gef-external-link-tagger] a",
      "script"   : "utilities/externalLinkTagger",
      "config"   : {
        "domains" : [
          "localhost",
          "education.nsw.gov.au",
          "policies.education.nsw.gov.au", 
          "my.education.nsw.gov.au", 
          "test.education.nsw.gov.au",
          "uat.education.nsw.gov.au", 
          "pre.education.nsw.gov.au"
        ]
      }
    },

    "catalogueForm" : {
      "selector" : "[data-gef-auto-submit]",
      "script"   : "components/catalogueForm"
    },

    "classToggle" : {
      "selector" : "[data-gef-class-toggle]",
      "script"   : "utilities/classToggle"
    },

    "tabs" : {
      "selector" : "[data-gef-tabs]",
      "script"   :  "components/tabs"
    },

    "SearchSPA" : {
      "selector"  : ".gef-search-spa",
      "script"    : "searchSPA",
      "singleton" : true
    },

    "HealthSafety" : {
      "selector"  : ".gef-health-safety",
      "script"    : "apps/health-safety/main",
      "singleton" : true
    },

    "BushfireRegister" : {
      "selector"  : ".gef-bushfire-register",
      "script"    : "apps/bushfire-register/main",
      "singleton" : true
    },

    "feedbackTrigger" : {
      "selector" : ".gef-feedback-trigger",
      "script"   : "components/feedbackTrigger"
    },

    "tagsTransform" : {
      "selector" : "[data-gef-tags]",
      "script"   : "utilities/tagTransform"
    },

    "ariaToggle" : {
      "selector" : "[data-gef-aria-toggle]",
      "script"   : "utilities/ariaToggler",
      "config"   : {
        "click_outside": true
      }
    },


    "noticeRibbonToggle" : {
      "selector" : "[data-gef-notice-ribbon]",
      "script"   : "utilities/ariaToggler",
      "config"   : {
        "auto_open": noticeOpenByDefault
      }
    },

    "globalHeaderToggle" : {
      "selector" : "[data-gef-global-header-search-toggle]",
      "script"   : "utilities/ariaToggler"
    },

    "anchorBox" : {
      "selector"  : "[data-gef-anchor-box]",
      "script"    : "components/anchorBox",
      "config"    : {
        "inject_after_me" : ".lead:first-of-type",
        "inject_backup": "h1:first-of-type"
      }
    },

    "secondaryAnchorBox-WithParent" : {
      "selector"  : ".gef-secondary-index-parent",
      "script"    : "components/secondaryAnchorBox",
      "config"    : {
        "inject_after_me" : ".gef-secondary-index-parent",
        "inject_backup": ".gef-secondary-index-position"
      }
    },

    "secondaryAnchorBox-WithoutParent" : {
      "selector"  : ".gef-secondary-index-position",
      "script"    : "components/secondaryAnchorBox",
      "config"    : {
        "inject_after_me" : ".gef-secondary-index-position",
        "inject_backup": ".gef-secondary-index-position"
      }
    },

    "anchorIndex" : {
      "selector"  : ".gef-LD-main",
      "script"    : "components/anchorIndex",
      "config"    : {
      }
    },

    "anchorIndexScroll" : {
      "selector" : "body",
      "script" : "utilities/scrollToggle",
      "config" : {
        "checkpoint" : ".gef-LD-main",
        "subject"     : ".gef-LD-index",
        "passed_attr" : "data-scroll-passed",
        "scroll_window" : true,
        "scroller_position" : "top"
      }
    },

    "guidedJourney" : {
      "selector" : ".gef-GJ-main",
      "script" : "components/guidedJourney",
      "config" : {
      }
    },

    "guidedJourneyScroll" : {
      "selector" : "body",
      "script" : "utilities/scrollToggle",
      "config" : {
        "checkpoint" : ".gef-GJ-main",
        "subject"     : ".gef-GJ-index",
        "passed_attr" : "data-scroll-passed",
        "scroll_window" : true,
        "scroller_position" : "top"
      }
    },

    "overlay" : {
      "selector"  : "[data-gef-dialogue-trigger]",
      "script"    : "components/dialogue",
      "config"    : {
      }
    },

    "submitOnEvent" : {
      "selector"  :  "#chkAllDept",
      "script"    :  "utilities/submitOnEvent",
      "config"    :   {
        "triggers"  : ["click", {"keypress":"enter"}],
        "submitForm": ".gef-main .gef-search"
      }
    },

    "globalLinkDrop": {
      "selector": ".gef-global-link-button",
      "script": "utilities/globalLinkDrop"
    },

    "checkFooter" : {
      "selector"   : ".gef-section-footer",
      "script"     : "utilities/checkFooter"
    },

    "searchIndexChecker" : {
      "selector"   : "body",
      "script"     : "utilities/searchIndexChecker",
      "config"     : {
        "checkOnLoad" : true,
        "environment" : "prod"
      },
      "singleton" : true,
      "test"      : function() {
        if (window.location.href.match("debug=true")) {
          return true
        }
        return false
      }
    }

  }
}

export default configuration
