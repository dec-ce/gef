import { SCHOOLS_GET_FULFILLED, SCHOOLS_GET_REJECTED, SCHOOLS_GET_REQUESTED } from "apps/health-safety/constants/actionTypes"
import axios from "axios";

export function getSchools() {
    return dispatch => {
        dispatch(getSchoolsRequestedAction());

        return axios.get(
            "https://doe-health-and-safety.firebaseio.com/schools.json"
        ).then(response => {
            const schools = response.data;
            dispatch(getSchoolsFulfilledAction(schools));
        }).catch((error) => {
            console.log(error);
            dispatch(getSchoolsRejectedAction());
        })
    }
}

function getSchoolsRequestedAction() {
    return {
        type: SCHOOLS_GET_REQUESTED
    };
}

function getSchoolsFulfilledAction(schools) {
    return {
        type: SCHOOLS_GET_FULFILLED,
        schools
    }
}

function getSchoolsRejectedAction() {
    return {
        type: SCHOOLS_GET_REJECTED
    }
}
