"use strict"
import * as $ from "jquery"
/**
 * globalLinkDrop.js Binds a custom link to the JIRA collection dialogue.
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class globalLinkDrop {


    /**
     * Generates the dropdown on button click
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     * @param {String} config.classes.dropdown - class name of the generated dropdown
     *
     */

    constructor(selector, config) {

        this.config = {
            classes: {
                dropdown: "gef-global-links-dropdown"
            }
        }

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }
        var dropDownClass = this.config.classes.dropdown

        //The login button click event triggers the dropdown
        $(selector).on("click", function(e) {
            e.stopPropagation();
            $("." + dropDownClass).toggle();
            $(selector).toggleClass('rotate');
            if ($(selector).hasClass('rotate')) {
                $(selector).attr("aria-expanded", "true");
            } else {
                $(selector).attr("aria-expanded", "false");
            }
        });

        $(document).on('click', function(e) {
            $("." + dropDownClass).hide()
             if ($(selector).hasClass('rotate')) {
                $(selector).toggleClass('rotate');
                $(selector).attr("aria-expanded", "false");
             }

        });

        //Hide the drop down after tabbing through the last list item
        $("." + dropDownClass + " li:last-child ul li:last-child").on('keydown', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which)

            if (code == 9 && e.shiftKey) { //keycode 9 is for tab
                        // do nothing
            } else if (code === 9) {
                $("." + dropDownClass).hide()
                if ($(selector).hasClass('rotate')) {
                    $(selector).toggleClass('rotate');
                    $(selector).attr("aria-expanded", "false");
                }
            }
        });

    }

}

export default globalLinkDrop