"use strict"
import * as $ from "jquery"
import AriaToggler from "utilities/ariaToggler"
import User from "core/user"

/**
 * Tabs.js manages the necessary changes in DOM structure between the mobile and tablet breakpoints.
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class Tabs {

  constructor(selector, config) {

    /**
     *
     * @param {string} config.breakpoint: the point at which the tabs change from accordions (mobile) to tabs (desktop)
     * @param {string} config.selectors.anchors: the links which activate the tabs.
     * @param {string} config.selectors.panels: the content sections which move in the DOM to create the tabs layout
     * @param {string} config.classes.active: the class to add to the selector to show js is active
     * @param {string} config.elements.tabletGrouping: the element(s) which wrap the content sections on desktop layout
     *
     */
    this.config = {
      breakpoint: "screen and (min-width: 768px)",
      selectors: {
        anchors:    ".js-tablist__link",
        panels:     "[role=tabpanel]"
      },
      classes: {
        active: "gef-tabs--active",
      },
      elements: {
        tabletGrouping: "<div class=\"gef-tabs__panel-group\"></div>"
      }
    }

    // // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }


    let self = this
    function checkBreakpoint(mql) {
      if (mql.matches) {
        // Tablet and up
        self.changeToDesktop()
      } else {
        // Mobile and up
        self.changeToMobile()
      }
    }

    // Check if selector has been passed to constructor
    if (selector) {
      // Use jQuery to match find the relevant DOM element(s)
      this.$container = $(selector)
      // add active class to container
      this.$container.addClass(this.config.classes.active)
      // jQuery obj for tablet panel grouping
      this.$tabletGrouping = $(this.config.elements.tabletGrouping)
      // jQuery obj for panels
      this.$panels = this.$container.find(this.config.selectors.panels);
      this.$anchors = this.$container.find(this.config.selectors.anchors);


      this.user = User.shared()

      // Check to see if the UserRoles cookie is not empty
      if (this.user.roles) {
        // Users can have multiple roles. We have no way to tell what the user's primary role is grab the first role
        let firstUserRole = this.user.roles[0]
        // Set the selected tab when first loaded when the page is first loaded
        this.setSelectedTab(firstUserRole)
      }


      // set the breakpoint matcher
      if (window.matchMedia === undefined) {
        // on ie9 just change to desktop version
        this.changeToDesktop()
      } else {
        this.match = window.matchMedia(this.config.breakpoint)
        // test the breakpoint at runtime
        checkBreakpoint(this.match)
        // listen for breakpoint changes
        this.match.addListener(checkBreakpoint)
      }
    }

    require(["jqueryAccessibleTabs"])
    
  }


  /**
   * Set the selected tab/panel based on the user's role
   * 
   * @param {String} userRole - User's role e.g. school.corporate
   */
  setSelectedTab(userRole) {
    let $selectedTab = this.$anchors.filter(`[data-selected]`)
    let $matchedTab = this.$anchors.filter(`[data-audience*="${userRole}"]`)
    let $defaultTab = this.$anchors.filter(`[data-audience*="default"]`)
    
    $selectedTab.removeAttr("data-selected")
    $selectedTab.removeAttr("aria-selected")
    $selectedTab.removeAttr("tabindex")

    // If we have a tab/panel who's audience matches the user's role then set that tab to be the selected tab
    if ($matchedTab.length > 0) {
      $matchedTab.attr("data-selected", 1)
    // If we don't have user role and audience match then we set the "default" tab/panel as the selected tab
    } else if ($defaultTab.length > 0) {
      $matchedTab.attr("data-selected", 1)
    }
  }


  changeToDesktop() {

    // wrap the panels in list items for semantics
    this.$panels.wrap("<div></div>")
    // Append to the tablet container
    this.$container.append(this.$tabletGrouping.append($(this.$panels.parent())))
    // Focus selection for accessibility
    
  }

  changeToMobile() {

    // Test to see if the tabs have been modified for desktop
    if (this.$container.find('.gef-tabs__panel-group').length) {

      let $anchors  = this.$container.find(this.config.selectors.anchors)

      // loop through anchors and add the panels after them
      $anchors.each(function(i) {
        $anchors.eq(i).after(self.$panels.eq(i))
      })

      // delete .get-tabs__panel-group
      this.$container.find('.gef-tabs__panel-group').remove()

    } else {
      // do nothing
    }
  }

}

export default Tabs