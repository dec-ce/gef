import { combineReducers } from "redux"
import providers from "apps/health-safety/reducers/providers"
import schools from "apps/health-safety/reducers/schools"
import searchinput from "apps/health-safety/reducers/searchinput"
import selectedschool from "apps/health-safety/reducers/selectedschool"
import invalidschool from "apps/health-safety/reducers/invalidschool"
import networkcode from "apps/health-safety/reducers/networkcode"
import selectedproviders from "apps/health-safety/reducers/selectedproviders"

export default combineReducers({
    providers,
    schools,
    searchinput,
    selectedschool,
    selectedproviders,
    invalidschool,
    networkcode
});