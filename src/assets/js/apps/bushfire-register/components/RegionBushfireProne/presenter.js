import React from "react"

const RegionBushfireProne = (props) => {
    const regionschools = props.regionschools

    if (regionschools) {
        return (
            <div id={"resultsRegion"} style={{display: "block"}}>

                <table>
                    <thead>
                        <tr>
                            <th>School</th>
                            <th>NSW fire area</th>
                            <th>Busfire prone</th>
                            <th>High risk</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            regionschools.map((school, key) => {

                                return (
                                    <tr key={key}>
                                        <td>{school.name}</td>
                                        <td>{school.nswFireArea}</td>
                                        <td>{school.bushfireProne ? 'Yes' : 'No'}</td>
                                        <td>{school.highRisk ? 'Yes' : 'No'}</td>
                                    </tr>
                                )
                            })
                        }
                        
                    </tbody>
                </table>
                
            </div>
        )
    } else {
        return ""
    }
}

export default RegionBushfireProne
