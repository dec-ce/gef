import {SEARCHINPUT_SET} from "apps/bushfire-register/constants/actionTypes"

export const setSearchInput = (searchinput) => {
    return dispatch => {
        dispatch({
            type: SEARCHINPUT_SET,
            searchinput
        })
    }
}