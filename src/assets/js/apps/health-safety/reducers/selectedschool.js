import * as actionTypes from "apps/health-safety/constants/actionTypes"

const initialState = null;

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SELECTEDSCHOOL_SET:
            return setSelectedSchool(state, action);
        default:
            return state;
    }
}

function setSelectedSchool(state, action) {
    const { selectedschool } = action;
    if (!selectedschool) {
        return null;
    } else {
        return selectedschool;
    }
}