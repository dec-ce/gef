import { createStore, applyMiddleware } from "redux"
import { createLogger } from "reduxLogger"
import thunk from "reduxThunk"
import rootReducer from "apps/bushfire-register/reducers/index"

const logger = createLogger()
const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore)

export default function configureStore(initialState) {
    return createStoreWithMiddleware(rootReducer, initialState)
}