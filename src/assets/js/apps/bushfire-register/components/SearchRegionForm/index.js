import {connect} from "reactRedux"
import lifecycle from "reactPureLifecycle"
import SearchForm from "apps/bushfire-register/components/SearchRegionForm/presenter"
import {setSelectedRegion} from "apps/bushfire-register/actions/selectedregion"
import {getBushfireProneRegion} from "apps/bushfire-register/actions/bushfireproneregion"

function mapStateToProps(state) {
    const regions = state.regions
    const bushfireprone = state.bushfireprone
    const selectedregion = state.selectedregion
    return {
        regions,
        bushfireprone,
        selectedregion
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetBushfireProneRegion: (selectedregion, bushfireprone) => dispatch(getBushfireProneRegion(selectedregion, bushfireprone)),
        onSetSelectedRegion: (selectedregion) => dispatch(setSelectedRegion(selectedregion))
    }
}

const componentWillMount = (store) => {
    store.onSetSelectedRegion("default")
}

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(SearchForm)

export default connect(mapStateToProps, mapDispatchToProps)(cwm)