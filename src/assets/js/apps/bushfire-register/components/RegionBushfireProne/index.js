import {connect} from "reactRedux"
import lifecycle from "reactPureLifecycle"
import RegionBushfireProne from "apps/bushfire-register/components/RegionBushfireProne/presenter"

function mapStateToProps(state) {
    const regionschools = state.regionschools
    return {
        regionschools
    }
}

const componentWillMount = (store) => {

}

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(RegionBushfireProne)

export default connect(mapStateToProps)(cwm)