"use strict"
import NotificationCentre from "core/notificationCentre"
import * as $ from "jquery"

class ComponentLoader {

  /**
   * Create a new ComponentLoader
   *
   * @param {PlainObject} themeConfig -
   * @param {String} themeConfig.name -
   * @param {PlainObject} themeConfig.components -
   *
   * @returns {Loader}
   */
  constructor(themeConfig) {

    // Get a reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    // Subscribe to diagnostic events broadcast by other classes
    this.notificationCentre.subscribe( "domUpdate", (containerElement) => this.update(containerElement) )

    // Check to make sure we have been supplied with a config
    if (themeConfig) {
      // If the themeConfig is a string representing a path to a config JSON file fetch the file
      if (typeof themeConfig === "string") {
        this.fetchConfig(themeConfig)
      } else {
        // Else load the components
        this.config = themeConfig
        this.loadComponents(themeConfig)
      }
    }
  }


  /**
   * Fetches a config file via an AJAX request and upon sucessful load will initiate the loading of components
   *
   * @param {String} configPath - Either a relative (to loader.js) or abosolute URL to the theme config
   **/
  fetchConfig(configPath) {
    let _self = this

    $.ajax({
      url: configPath,
      success: function(data, status, xhr) {
        _self.loadComponents(data)
      },
      error: function(xhr, status, error) {
        console.log("Failed to load theme config.", error)
      }
    })
  }


  /**
   * Load any components found in a specified container element. Relies on theme config already having been loaded
   * 
   * @param {String|Element|jQuery} containerElement - Either a CSS selector, DOM element or matched jQuery object
   */
  update(containerElement) {
    if (!this.config) {
      throw new Error("Theme config missing. Please load a config by supplying a config path/object to the constructor or the loadComponents() function.")
    }

    this.loadComponents(this.config, containerElement)
  }


  /**
   * Load 1 or more componets specified
   *
   * @param {PlainObject|String} themeConfig - Theme config object or a path to a config JSON file
   * @param {String} themeConfig.name - The name of the theme
   * @param {PlainObject} themeConfig.components - A POJO with 1 or more components defined with user specified key and the components config as the value
   */
  loadComponents(themeConfig, containerElement) {

    // If the themeConfig is a string representing a path to a config JSON file fetch the file
    if (typeof themeConfig === "string") {
      this.fetchConfig(themeConfig)
      return
    }

    this.config = themeConfig

    for (let componentKey in themeConfig.components) {
      this.loadComponent(themeConfig.components[componentKey], containerElement)
    }
  }


  /**
   * Load a single component
   *
   * @param {PlainObject} componentConfig -
   * @param {String} componentConfig.selector -
   * @param {String} componentConfig.script -
   * @param {PlainObject} componentConfig.config -
   * @param {Function} componentConfig.test -
   * @param {Boolean} componentConfig.singleton -
   */
  loadComponent(componentConfig, containerElement) {
    if (!componentConfig) {
      return new Error("Could not load component. Component config is required")
    }

    // Deafult component config model
    let componentDefaults = {
      selector: null,
      script: null,
      config: null,
      test: null,
      singleton: false
    }

    // Merge componentDefinition into the default component
    componentConfig = $.extend(componentDefaults, componentConfig)

    // Don't attempt to load the component definition if it doens"t have a
    // "selector" property set
    if (componentDefaults.selector === undefined) {
      return new Error("Could not load component. The component's selector property was not set")
    }

    // Don't attempt to load the component definition if it doens"t have a
    // "script" property set
    if (!componentConfig.script) {
      return new Error("Could not load component. The component's script property was not set")
    }

    // If the component has a test callback and the callback returns false
    // then don't load the component
    if (componentConfig.test !== null && componentConfig.test() === false) {
      return false
    }
    
    let $presentComponents

    // If containerElement is a build a new CSS selector string that scopes the componentConfig.selector to the containerElement
    if (typeof containerElement === "string") {
      $presentComponents = $(`${containerElement} ${componentConfig.selector}`)

    // If containerElement is a HTML element or matched jQuery object then use the "context" param of $ to scope the componentConfig.selector to the containerElement
    } else if (containerElement instanceof Element || containerElement instanceof $) {
      $presentComponents = $(componentConfig.selector, containerElement)

    // Otherwise perform a jQuery match that is globaly scoped i.e. the whole page
    } else {
      $presentComponents = $(componentConfig.selector)
    }

    // Find all the compontents with matching our compontents selector string
    // let $presentComponents = $(componentConfig.selector)

    // If the component is not present in the DOM then don't load the component
    if ($presentComponents.length < 0) {
      return false
    }

    let _self = this

    // Loop through each instance of the compontent that jQuery found
    $.each($presentComponents, function(index, componentInstance) {

      let promisedCompontent = _self.getComponent(componentConfig.script)

      promisedCompontent.done(function(ComponentClass) {
        _self.initComponent(ComponentClass, componentInstance, componentConfig)
      })

      promisedCompontent.fail(function(error) {
        console.log(error, "Unable to load component.", componentConfig)
      })

    })
  }


  /**
   * Instantiate a new GEF module
   *
   * @param {Class} ComponentClass - a URL/path to a AMD compliant JS module
   * @param {jQuery} componentInstance - The element to pass to the ComponentClass as the first argument
   * @param {componentConfig} - The component config definition to pass to the ComponentClass as the second argument
   *
   * @returns {Object|Error} Returns an instance of the passed Class or an Error object
   */
  initComponent(ComponentClass, componentInstance, componentConfig) {

    try {

      // Ideally typeof ComponentClass should be of type 'object' but because AMD modules are wrapped in a define function it returns a type of 'function'
      if (typeof ComponentClass === "function") {
        let component

        if (componentConfig.singleton) {
          // If the component should be instantiated as a singleton

          if (componentConfig.hasOwnProperty("args")) {
            component = ComponentClass.shared.apply(componentConfig.args)
          } else {
            component = ComponentClass.shared(componentInstance, componentConfig.config)
          }

        } else {
          // else instatiate as a regular class

          if (componentConfig.hasOwnProperty("args")) {
            component = new ComponentClass(...componentConfig.args)
          } else {
            component = new ComponentClass(componentInstance, componentConfig.config)
          }

        }

        return component

      } else {
        return new Error(`Error loading component: ${componentConfig.script}`)
      }

    } catch (error) {
      return new Error(`Error loading component: ${componentConfig.script}`)
    }

  }


  /**
   * Get a JS AMD compliant module using RequireJS
   *
   * @param {String} scriptURL - a URL/path to a AMD compliant JS module
   *
   * @returns {Promise} Returns a jQuery Promise which resolved to a RequireJS's sucess callback response
   */
  getComponent(scriptURL) {
    let $deferred = $.Deferred()

    // Use RequireJS require() function to
    require(
      // Use RequireJS to load the compontent JS file
      [scriptURL],
      // On success
      function(ComponentClass) {
        $deferred.resolve(ComponentClass)
      },
      // On error
      function(error) {
        $deferred.reject(error)
      }
    )

    // Return a jQuery Promise
    return $deferred.promise()
  }

}

export default ComponentLoader
