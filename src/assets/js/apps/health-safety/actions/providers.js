import { PROVIDERS_GET_FULFILLED, PROVIDERS_GET_REJECTED, PROVIDERS_GET_REQUESTED } from "apps/health-safety/constants/actionTypes"
import axios from "axios"

export function getProviders() {
    return dispatch => {
        dispatch(getProvidersRequestedAction());

        return axios.get(
            "https://doe-health-and-safety.firebaseio.com/providers.json"
        ).then(response => {
            const providers = response.data;
            dispatch(getProvidersFulfilledAction(providers));
        }).catch((error) => {
            console.log(error);
            dispatch(getProvidersRejectedAction());
        })
    }
}

function getProvidersRequestedAction() {
    return {
        type: PROVIDERS_GET_REQUESTED
    };
}

function getProvidersFulfilledAction(providers) {
    return {
        type: PROVIDERS_GET_FULFILLED,
        providers
    }
}

function getProvidersRejectedAction() {
    return {
        type: PROVIDERS_GET_REJECTED
    }
}
