import React from "react"

const BushfireProne = (props) => {
    const selectedschools = props.selectedschools

    if (selectedschools) {
        return (
            <div id={"results"} style={{display: "block"}}>
                <div className={"gef-call-out-box"} id={"bf_results_text"}>
                    {
                        selectedschools.map((school, key) => {
                            if (school.highRisk) {
                                return (
                                    <p key={key}>{school.name} in the NSW fire area of {school.nswFireArea} is bushfire prone. It
                                        is at <strong>increased risk</strong> in the event of bushfire and is on the
                                        department"s bushfire register.</p>
                                )
                            } else if (school.bushfireProne) {
                                return (
                                    <p key={key}>{school.name} in the NSW fire area of {school.nswFireArea} is <strong>bushfire
                                        prone.</strong></p>
                                )
                            } else {
                                // Actually this should never happen
                                return (
                                    <p key={key}>{school.name} is <strong>not</strong> in an increased risk or bushfire-prone
                                        area.
                                    </p>
                                )
                            }
                        })
                    }
                </div>
            </div>
        )
    } else {
        return ""
    }
}

export default BushfireProne
