"use strict"
import placeholder from "placeholder/jquery.placeholder"
import * as $ from "jquery"

class IE9Placeholder {

  constructor(selector, config) {
    $('input, textarea').placeholder()
  }

}

export default IE9Placeholder
