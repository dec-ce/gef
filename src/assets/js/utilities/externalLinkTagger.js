"use strict"
import * as $ from "jquery"

/**
 * Create a new LinkTagger
 *
 * @example
 * // instantiate and check/tag the passed link using class constructor
 * let tagger = new LinkTagger("a", { domains: ["education.nsw.gov.au"] })
 *
 * @example
 * // Check/tag the passed link using tag function
 * let tagger = new LinkTagger()
 * tagger.tag("a", { domains: ["education.nsw.gov.au"] })
 *
 * @class
 * @requires jQuery
 */
 class LinkTagger {

  /**
   * @param {(String|jQuery)} selector - A CSS selector or jQuery object
   * @param {Object} config - Object literal
   * @param {string} config.selectors.externalIcon - CSS selector used in the config.template
   * @param {array} config.domains - Array of domain names (strings) to be white listed as "internal" domains
   * @param {string} config.template - A snippet of HTML to append to end of the anchor(s)
   *
   * @returns {LinkTagger} Returns a LinkTagger object
   */
  constructor(selector, config) {

    this.config = {
      selectors: {
        externalIcon: ".gef-external-link"
      },
      domains: [],
      template: "<span class=\"gef-external-link\"></span>"
    }

    // Merge default config with passed config
    this.setConfig(config)

    // Check if selector has been passed to constructor
    if (selector) {
      this.tag(selector)
    }

  }


  /**
   * Merge default config with passed config
   *
   * @param {Object} config - Object literal
   * @param {string} config.selectors.externalIcon - CSS selector used in the config.template
   * @param {array} config.domains - Array of domain names (strings) to be white listed as "internal" domains
   * @param {string} config.template - A snippet of HTML to append to end of the anchor(s)
   *
   * @returns {Obejct} reference to instance config
   */
  setConfig(config) {
    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    return this.config
  }


  /**
   * Check if an anchor or array of anchors are external links and tag any external links with an icon
   * Paramters are identical to those expected by the class constructor
   *
   * @see {@link constructor}
   */
  tag(selector, config) {
    this.setConfig(config)

    // Add the current domain to the array of white listed domains
    this.config.domains.push(window.location.host)

    // Use jQuery to find the relevant selector in the DOM
    this.$selector = $(selector)

    let _self = this

    // Loop through each anchor
    this.$selector.each(function(index, anchor) {
      let $anchor = $(anchor)
      let isLocal = true

      // Loop through each of our white listed domains
      $.each(_self.config.domains, function(i, domain) {
        // If href doesn't contain the domain
        if (_self.check($anchor, domain)) {
          isLocal = false
        }
      })

      // If the href doesn't match any of the white listed domains then tag it
      if (isLocal) {
        _self.addTag($anchor, _self.config.template)
      }

    })

  }


  /**
   * Appends the passed HTML to an anchor  first checking if the anchor has already been flagged as external
   *
   * @param {jQuery} $anchor - An 'a' element encapuslated in a jQuery object
   * @param {string} template - HTML to be appended to end of anchor tag
   *
   * @returns {jQuery} The orignal 'a' element passed into the function
   */
  addTag($anchor, template) {
    if ($anchor.find(this.config.selectors.externalIcon).length == 0) {
      // Appends class gef-externalLink to the link if host does not match list of hosts above
      $anchor.append(template)

      // Adds an attribute target="_blank" if the external link is found
      // $anchor.attr("target","_blank") // removed GXF-655
    }

    return $anchor
  }


  /**
   * Check if an anchor tag's href contains the passed domain or is a mailto: email
   *
   * @param {jQuery} $anchor - An 'a' element encapuslated in a jQuery object
   * @param {string} domain - The domain to check
   *
   * @returns {boolean} True is returned if the anchor's href contains the passed domain else returns false
   */
  check($anchor, domain) {
    let url = $anchor[0].href

    // Check if this URL does not contain the domain or is a mailto:
    if (url.search(domain) >= 0 || url.search("mailto:") >= 0) {
      return true
    } else {
      return false
    }
  }

}

/**
 * A module for falging links as "external links" (via an icon appened to a link).
 * Module will compare the links href against a white list of "internal" domains.
 * By default all hrefs are compared against the current domain i.e. window.location.host
 *
 * @module utilities/externalLinkTagger
 */
export default LinkTagger
