"use strict"
import SocialMediaAccount from "./account"
import SocialMediaPost from "./post"
import NotificationCentre from "core/notificationCentre"
import Environment from "core/environment"
import * as $ from "jquery"

/**
 * Connects to the Social Media Aggregator microsevice and displays a feed of social media posts from social media accounts defined via the config object
 *
 * @since 1.1.31
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
*
 * @requires SocialMediaAccount:components/socialMedia/account
 * @requires SocialMediaPost:components/socialMedia/post
 * @requires NotificationCentre:core/notificationCentre
 * @requires Environment:core/environment
 * @requires jQuery
 */
class SocialMedia {

  /**
   * Creates a new SocialMedia object
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   *
   * @example
   * // Instantiate a new SocialMedia
   * let SocialMedia = new SocialMedia(".gef-social-media", { accounts: [{ type:"facebook", username: "digservdoe", numberOfPosts: 2 }] })
   */
  constructor(selector, config) {

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    // Default class config options
    this.config = {
      environment: "nonProd",
      key: "LpBijiJPrF85ZimBlVPEz4RjVkCLHmWA6IwAAaWP",
      debug: false,
      selectors: {
        loader: ".gef-loader"
      }
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Set up a instance level reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    // Use jQuery to match find the relevant DOM element(s)
    this.$container = $(selector)
    this.$list = this.$container.find("ul")
    this.$loadingIndicator = this.$container.find(this.config.selectors.loader)

    this.accounts = new Array()
    this.posts = new Array()

    // Get the social media config stored in the data-gef-social-media attribute of the container div (this.$container)
    this.getConfiguration()

    // Initiate a request to the Social Media microservice for the latest social media posts
    this.getPosts()

  }


  /**
   * Load the component config from data-gef-social-media attribute. The config MUST be written in valid JSON syntax.
   */
  getConfiguration() {

    // Grab the config (which needs to be written in valid JSON syntax) from the $container's data-gef-social-media attribute
    let config = this.$container.data("gefSocialMedia")

    // Check to make sure the config defined in the data-gef-social-media attribute is valid JSON by checkting to see if the jQuery.data() function did not return a String
    if (typeof config !== "string") {

      this.displayCap = config.displayCap
      this.contentLength = config.contentLength || null
      this.minimalist = config.minimalist || false
      this.dateplacement = config.dateplacement || false
      console.log("dateplacement"+ config.dateplacement);
      // Diagnostic update
      this.notificationCentre.publish("diagnosticUpdate", {
        module: "SocialMedia",
        messages: [
          { text: `Display cap`, variable: this.displayCap },
          { text: `Content max length`, variable: this.contentLength }
        ]
      })

      // Loop through the social media service accounts defined in the config
      for (let step = 0; step < config.services.length; step++) {

        let service = config.services[step]
        let socialMediaAccountConfig = new SocialMediaAccount(service.service, service.username, service.numberOfPosts)

        // If the service config has a type defined set the socialMediaAccountConfig.type
        socialMediaAccountConfig.type = service.type || null

        // Add the socialMediaAccountConfig
        this.accounts.push(socialMediaAccountConfig)

        // Diagnostic update
        this.notificationCentre.publish("diagnosticUpdate", {
          module: "SocialMedia",
          messages: [
            { text: `Account config`, variable: socialMediaAccountConfig.toString() }
          ]
        })
      }

      // Diagnostic update
      this.notificationCentre.publish("diagnosticUpdate", {
        module: "SocialMedia",
        messages: [
          { text: `Number of account configs`, variable: this.accounts.length }
        ]
      })

    // Looks like the data-gef-social-media attribute contents are malformed. Likely the JSON syntax is not written correclty.
    } else {
      // Diagnostic update
      this.notificationCentre.publish("diagnosticUpdate", {
        module: "SocialMedia",
        messages: [
          { text: `Number of account configs`, variable: this.accounts.length },
          { text: `Config defined in data-gef-social-media attribute is malformed` }
        ]
      })
    }

  }


  /**
   * Construct the jQuery.ajax() "data" option payload based on the social media account config(s)
   *
   * @returns {Object} - The paramaters to be sent via the jQuery ajax request
   */
  buildPayload() {
    let payload = {
      socialmediatype: "all",
      numberoffeeds: 1
    }

    // If we only have one social media account defined overwrite the value of socialmediatype attribute with the type specified in the config
    if (this.accounts.length == 1) {
      payload.socialmediatype = this.accounts[0].service
    }

    // Loop through all the social media accounts defined in the config
    for (let step = 0; step < this.accounts.length; step++) {

      let socialMediaProfile = this.accounts[step]
      // Merge social media service specific parameters with the exisitng payload
      payload = $.extend(payload, socialMediaProfile.toMicroserviceParam())
      // As we loop through each account definition we check for the highest numberOfPosts
      if (socialMediaProfile.numberOfPosts > payload.numberoffeeds) {
        payload.numberoffeeds = socialMediaProfile.numberOfPosts
      }

    }

    return payload
  }


  /**
   * Show or hide the loading indicator
   *
   * @param {Boolean} show - Default value is true. "true" value shows the indicator, "false value hides the indicator"
   */
  showLoadingIndicator(show = true) {
    if (show) {
      this.$loadingIndicator.attr("aria-hidden", "false")
    } else {
      this.$loadingIndicator.attr("aria-hidden", "true")
    }
  }


  /**
   * Called when the AJAX request to retrieve the latest posts from the Social Media microservie returns with a HTTP 200 response
   *
   * @param {Object} data - Web service JSON response transformed to a Javascript object
   * @param {String} textStatus - HTTP status
   * @param {jqXHR} jqXHR - jQuery superset of XMLHttpRequest
   */
  successHandler(data, textStatus, jqXHR) {
    // Diagnostic update
    this.notificationCentre.publish("diagnosticUpdate", {
      module: "SocialMedia",
      messages: [
        { text: `AJAX request success` },
        { text: `Number of posts returned`, variable: data.feedEntries.length }
      ]
    })

    // Hide the loading indicator
    this.showLoadingIndicator(false)

    // Instatiate a counter to track how many posts we add tot he DOM/display to the user
    let count = 1
    console.log("number of results", data.feedEntries.length)
    // Loop through the returned posts
    for (let step = 0; step < data.feedEntries.length; step++) {
      let post = data.feedEntries[step]
      // Instantiate a new SocialMediaPost object passing the "raw" object for each post
      let socialMediaPost = new SocialMediaPost(post)
      // Add the SocialMediaPost object to our array of returned posts
      this.posts.push(socialMediaPost)

      // If the counter is <= to our display cap for posts add the post to the DOM and increment the counter by 1
      if (count <= this.displayCap) {
        this.$list.append(socialMediaPost.toHTML(this.contentLength, this.minimalist, this.dateplacement))
        count++
      }

    }

  }


  /**
   * Called when the Social Media microservice returns a HTTP reponse that isn't 200 OK. This occurs when there is an error retriving the social media posts for some reason.
   *
   * @param {jqXHR} jqXHR - jQuery superset of XMLHttpRequest
   * @param {String} textStatus - HTTP status
   * @param {Error} errorThrown - Javascript Error object
   */
  errorHandler(jqXHR, textStatus, errorThrown) {
    // Hide the loading indicator
    this.showLoadingIndicator(false)

    // Display an error to the user
    this.$container.append(`<p><i class="ui-icon-warning"></i> Error retrieving social media feed</p>`)

    // Diagnostic update
    this.notificationCentre.publish("diagnosticUpdate", {
      module: "SocialMedia",
      messages: [
        { text: `AJAX request failed`, variable: errorThrown }
      ]
    })
  }


  /**
   * Initiate a AJAX request to the Social Media microservice
   */
  getPosts() {

    // Show the loading indicator
    this.showLoadingIndicator(true)

    // Request the latest posts from social media accounts specified in the component config
    let ajaxRequest = $.ajax({
      method: "GET",
      url: Environment.get(this.config.environment, "microservices") + "/social-media/prod",
      dataType: "json",
      headers: {
        "x-api-key": this.config.key
      },
      data: this.buildPayload(),
      success: this.successHandler.bind(this),
      error: this.errorHandler.bind(this)
    })

    // Diagnostic update
    this.notificationCentre.publish("diagnosticUpdate", {
      module: "SocialMedia",
      messages: [
        { text: `Request to Social Media microservice sent`, variable: Environment.get(this.config.environment, "microservices") + "/social-media/prod" },
        { text: `Request parameters`, variable: JSON.stringify(this.buildPayload()) }
      ]
    })

  }


  /**
   * Static function to instatiate the SocialMedia class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {ExampleClass} Reference to the same ExampleClass instatiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new SocialMedia(selector, config)
  }

}


/**
 * Exports the SocialMedia class as a module
 * @module
 */
export default SocialMedia
