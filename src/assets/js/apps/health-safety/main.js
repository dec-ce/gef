"use strict"
import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "reactRedux"
import configureStore from "apps/health-safety/store/configureStore"
import App from "apps/health-safety/App"


/**
 * Health and Safety training providers single page app
 *
 * @since 1.2.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2018 State Government of NSW
 *
 * @class
 */
class HeathSafety {

  constructor(selector, config) {
    

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    this.container = selector

    // Default class config options
    this.config = {}

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    console.log(selector, this.container)

    const store = configureStore();

    ReactDOM.render(
      <Provider store={store}>
        <App/>
      </Provider>,
      this.container
    )

  }


  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new HeathSafety(selector, config)
  }

}

/**
 * Exports the HeathSafety class as a module
 * @module
 */
export default HeathSafety