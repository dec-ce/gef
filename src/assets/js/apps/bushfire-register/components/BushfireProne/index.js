import {connect} from "reactRedux"
import lifecycle from "reactPureLifecycle"
import BushfireProne from "apps/bushfire-register/components/BushfireProne/presenter"

function mapStateToProps(state) {
    const selectedschools = state.selectedschools
    return {
        selectedschools
    }
}

const componentWillMount = (store) => {

}

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(BushfireProne)

export default connect(mapStateToProps)(cwm)