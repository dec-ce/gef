"use strict"
import * as $ from "jquery"

/**
  * Loops through A to Z list and makes unused tabs inactive.
  *
  * @since 0.4.0
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2015 State Government of NSW 2015
  *
  * @class
  * @requires jQuery
  */

class AnchorFilter {

  constructor(selector, config)  {

  /**
    *
    * @param {string} config.container: selector for the A to Z ordered list.
    * @param {string} config.inactive_class: The class that gets added to the tabs that are missings hrefs.
    *
    */

    this.config = {
      "container": selector,
      "inactive_class": "inactive"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Find all of the anchor elements within the container and store in variable.
    this.anchors = $(this.config.container).find('a')

    // Error message displayed if no anchor elements exist.
    if (this.anchors.length == 0) {
      console.warn("AnchorFilter: cannot find the anchors within selector: " + selector)
      throw new RangeError("Couldn't find any anchors")
    }

    // Call testAnchors function
    this.testAnchors()

  }

  /**
  * Finds and tests the Anchors
  *
  * @testAnchors
  *
  */
  testAnchors() {
    // Stores contents of 'this' in new variable so there is no clash with the function below.
    var _this = this
    this.anchors.each(function(i) {
      // Checks to see if the href attribute has a corresponding element. If it doesn't, calls makeInactive function
      var id = $(this).attr("href")
      if ($(id).length == 0) {
        _this.makeInactive(this)
      }
    })
  }

  /**
  * Make Anchors without corresponding elements inactive
  *
  * @testAnchors
  *
  */
  makeInactive(element) {
    var $el     = $(element),
        $parent = $el.parent()
    // Add inactive class
    $el.addClass(this.config.inactive_class)
    // Change anchor to span
    var html = $parent.html(),
        span = html.replace('<a', '<span').replace('</a', '</span')
    $parent.empty().append(span)
    // Remove the href value
    $parent.find('span').removeAttr('href')
  }
}

export default AnchorFilter

