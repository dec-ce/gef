import {NETWORK_CODE_GET_FULFILLED} from "apps/health-safety/constants/actionTypes"

const initialState = [];

export default function(state = initialState, action) {
    switch (action.type) {
        case NETWORK_CODE_GET_FULFILLED:
            return setNetworkCode(state, action);
        default:
            return state;
    }
}

function setNetworkCode(state, action) {
    const { networkcode } = action;
    if (networkcode === null) {
        return state;
    } else {
        return networkcode;
    }
}