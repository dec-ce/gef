"use strict"
import services from "apps/search/services/services"

/**
 * search.js
 * sets the primary search service :searchService
 * this service checks for keywords in the input box. If not there checks for queryStrings termed txtSearch, as this is the existing term from GEF searches
 * runs the http call to the GSA service using jsonp
 * Results are put into Results
 * summary results are recorded in nav
 */
services.service("searchService", ["$http", "$q",  "configService", "utilsService", function($http, $q,  configService, utilsService) {
  /**
  * Get the value of a querystring
  * @param  {String} field The field to get the value of
  * @param  {String} url   The URL to get the value from (optional)
  * @return {String}       The field value
  */

  this.results = []
  this.navigation = []
  this.searchTerm =  utilsService.searchTerm
  this.pageNumber = 1
  this.previousPage = 0
  this.queryTerm = 0
  this.currentPage = window.location.href
  this.browserAgent = window.navigator.userAgent
  this.error = false
  this.siteSearch = ""
  this.searchUrl = ""
  this.searchSiteChosen = utilsService.siteUrl
  this.searchSiteName = utilsService.siteName || configService.siteName

  let _self = this

  // main function that does the ajax call to the GSA page.
  // only returns the items that are shown on the page at that time.
  // nav lets you know the full number of responses, which is then passed into the pagination controllers
  // queryTerm is used to tell the display that a new search query has finished.
  // this will be set to a random number so if it changes whether page change or otherwise.

  this.getResults = function(keyword, pageNumber, siteSearch, siteName){
    pageNumber = pageNumber || _self.pageNumber
    siteSearch = siteSearch || _self.searchSiteChosen
    _self.searchSiteName = siteName || _self.searchSiteName
    // makes sure that listeners do not respond until the ajax call has a response.
    let deferred = $q.defer();
  //  if (pageNumber != 1) {
    //  _self.previousPage = pageNumber
    //}

    if (keyword) {
      _self.searchTerm = keyword
      // Google requires start at 0.
      pageNumber -= 1
      let startingNumber = pageNumber * configService.resultsPerPage

      let requestOptions = {
        params: configService.url.params
      }

      angular.extend(requestOptions.params, {
        query: keyword,
        start: startingNumber,
        num: configService.resultsPerPage,
        as_sitesearch: siteSearch
      })

      $http.jsonp(configService.url.base, requestOptions).then(
        function(data) {
          _self.navigation = data.data.results_nav
          _self.searchTerm = data.data.query.split(' site:')[0] //having to hack this because of dodgy return data
          _self.queryTerm = Math.random()
          _self.error = false
          if (data.data.results) {
            _self.tmpresults = data.data.results
          _self.results = []
          // need to see if these items are secure or not, and if so, change the values.
          angular.forEach(_self.tmpresults, function(value, key) {
            if (value.meta_tags.filter(function(item) { return item.name === "DCTERMS.type" })[0]) {
              //html page - check for staff only.
              let summary = value.summary
              let ispublic = value.meta_tags.filter(function(item) { return item.name === "document.public" })
              if (ispublic[0] && ispublic[0].value == "no") {
                summary = '<span class="link-tag-container"><span class="link-tag-icon" aria-hidden="true">STAFF ONLY</span><span class="show-on-sr">STAFF ONLY</span></span><br />'
                let description = value.meta_tags.filter(function(item) { return item.name === "document.description" })
                if(description[0]) {
                  summary += description[0].value
                } else {
                  summary += value.summary
                }
              }
              summary = summary.replace(/STAFF/g, "")
              summary = summary.replace(/ONLY/g, "")
              summary = summary.replace('<span class="link-tag-container"><span class="link-tag-icon" aria-hidden="true"> </span><span class="show-on-sr"> </span></span><br />', '<span class="link-tag-container"><span class="link-tag-icon" aria-hidden="true">STAFF ONLY</span><span class="show-on-sr">STAFF ONLY</span></span><br />')
              value.summary = summary
              _self.results.push(value)
            } else {
              // want to check whether secure - if so has "staff only" in the keywords
              let newvalue = value
              let keyword = value.meta_tags.filter(function(item) { return item.name === "Keywords" })
              if (keyword[0]) {
                let keywords = keyword[0].value.toLowerCase().split(/(,\s*)/)
                if (keywords.indexOf("staff only") > -1) {
                  newvalue.summary = '<span class="link-tag-container"><span class="link-tag-icon" aria-hidden="true">STAFF ONLY</span><span class="show-on-sr">STAFF ONLY</span></span><br />'

                  let description = value.meta_tags.filter(function(item) { return item.name === "Description" })
                  let subject = value.meta_tags.filter(function(item) { return item.name === "Subject" })
                  let comments = value.meta_tags.filter(function(item) { return item.name === "Comments" })
                  if(description[0]) {
                    newvalue.summary += description[0].value
                  } else if (subject[0]) {
                    newvalue.summary += subject[0].value
                  } else if (comments[0]) {
                    newvalue.summary += comments[0].value
                  }
                  newvalue.summary = newvalue.summary.replace("STAFF ONLYSTAFF ONLY", "")
                  _self.results.push(newvalue)
                  //console.log(value)
                } else {
                  _self.results.push(newvalue)
                }
              } else {
                _self.results.push(newvalue)
              }
            }

            //_self.results.push(value)
          })
        } else {
          _self.results = []
        }
        deferred.resolve(_self.queryTerm)  // lets listeners know that the query has finished.
        document.getElementById("search-results__summary").focus()                             // Set focus to the right location.
        },
        function(error) {
          _self.navigation = []
          _self.results = []
          _self.queryTerm = Math.random()
          _self.error = true
          console.log("error occurred", error)
        }
      )

    }

    // lets listeners know that something is happening
    return deferred.promise
  }

  // function that calls a matrix page and registers that an error occurred.
  // pass through the errorTerm for future flexibility (should be the same as queryTerm)
  this.registerError = function(errorTerm, errorPage, errorCurrentPage, errorBrowserAgent) {
    let requestOptions = {
      params: {
        errorword: errorTerm,
        errorpage: errorPage,
        errorcurrent: errorCurrentPage,
        browseragent: errorBrowserAgent
      }
    }
    $http.jsonp(configService.reportError.url, requestOptions).then()
  }

  // this.getRandomQuote = function() {
  //   let quotes = configService.quotes
  //   let index = Math.floor(Math.random() * quotes.length)
  //   return quotes[index]
  // }
  // return this

}])
