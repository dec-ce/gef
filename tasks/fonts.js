"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // Fonts found in src/assets/fonts
    return gulp.src(options.paths.dev.assets.fonts)
      .pipe(gulp.dest(options.paths.build.assets.fonts))
      .on("error", errorHandler)
  }
}
