"use strict"
import * as $ from "jquery"
import KeyCodes from "keycodes"
import NotificationCentre from "core/notificationCentre"

/**
 * Class Track downloads using Google Analytics
 * 
 * @since 1.3.4
 * 
 * @author Alex Motyka <alex.motyka@det.nsw.edu.au>
 * @copyright © 2018 State Government of NSW 2015
 * 
 * @requires jQuery
 * @requires ga
 * @requires KeyCodes
 * @requires NotificationCentre
 */
class GADownloadTracker {

  /**
   * Create a new GADownloadTracker
   * 
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   * @param {Array} config.events - 
   * @param {Object} config.payload.category - 
   * @param {Object} config.payload.action -
   * @param {Object} config.payload.label - 
   * @param {Object} config.fileTypes - an Object of key/value pairs where the key is the file type category and the value is an array of file type extensions
   * 
   * @example
   * // Instantiate a new GADownloadTracker
   * let gaDownlaodTracker = new GADownloadTracker( "a", { events: [ "click" ], fileTypes: { "Image": ["gif", "jpg", "jpeg", "png"] } )
   */
  constructor(selector, config) {

    // If the Google Analytics ga() function is not defined then return an error becuase we can't track downlaods without it
    if (typeof ga === "undefined") {
      return new Error("Google Analytics")
    }

    // Set up a instance level reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    this.$elements = $(selector)
    this.types = new Object()

    // Global config
    this.config = {
      events : [ "click", "contextmenu", KeyCodes.enter ],
      payload: {
        category : "File download",
        action : null,
        label : null
      },
      fileTypes : {
        "Image": ["gif", "jpg", "jpeg", "png"],
        "Word": ["doc", "docx", "dotx", "dot", "dotm", "docm", "rtf"],
        "PDF": ["pdf"],
        "Document": ["rotx", "pub", "txt", "ppt", "pptx", "pps", "pptm", "xps", "xls", "xlsx", "xlsm", "xml", "xlsb"],
        "Video": ["flv", "wmv", "avi", "mkv", "mp4"],
        "Audio": ["mp3", "ogg", "wav", "wma"]
      }
    }

    // Regex to find a file name extension in a URI
    this.suffixRegex = /\.([a-z0-9]*)(\?|#|$)/i

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Convert the fileTypes config dictionary into a more useful data structure
    this.types = this.buildTypeDictionary(this.config.fileTypes)

    // Filter the anchors so we only have the anchors who's href contains specific file name extensions 
    this.$elements = this.filter(this.$elements, this.types)

    // Add GA tracking to the links/anchors
    this.bindTracking(this.$elements, this.config.events, this.config.payload)

  }


  /**
   * Transforms the config.fileType dictionary into a flatter inverted data structure that maps file extensions to file type categories
   * 
   * @param {Object} types - an Object of key/value pairs where the key is the file type category and the value is an array of file type extensions
   * @returns {ExampleClass} an Object of key/value pairs where the key is the file extension and the value is the file type category
   * 
   * @example
   * let fileTypes = {
   *   "Image": ["gif", "jpg", "jpeg", "png"]
   * }
   * 
   * buildTypeDictionary(fileTypes)
   * 
   * // returns
   * // {
   * //   gif: "Image",
   * //   jpg: "Image",
   * //   jpeg: "Image",
   * //   png: "Image"
   * // }
   */
  buildTypeDictionary(types) {

    let typesObj = new Object()

    // Loop through each file type category
    for (const TYPE_CATEGORY in types) {
      
      let extensions = types[TYPE_CATEGORY]
      // Loop through each file type extension/suffix
      for (let i = 0; i<extensions.length; i++) {
        // Add a new key value pair mapping a suffix to a file type category
        typesObj[extensions[i]] = TYPE_CATEGORY
      }
    }

    return typesObj
  }


  /**
   * Accepts a jQuery object of anchor elements and returns a array of anchor elements that match contain a file name extension/suffix found in the supplied  href 
   * 
   * @param {Object} $elements - jQuery object of anchor elements
   * @param {Object} types - an Object of key/value pairs where the key is the file extension and the value is the file type category
   * @returns {Object} A jQuery object of anchor elements 
   */
  filter($elements, types) {
    
    // Use the map() fucniton to "loop" through the jQuery matched anchor elements
    let $newAnchors = $elements.map((index, anchor) => {

      // Use a regex to find the suffix/file name extension
      const MATCH = this.suffixRegex.exec(anchor.href)
      // MATCH[1] is the resulkt of a regex sub group
      const SUFFIX = MATCH[1]

      // If the file extension is present in the types Object return the anchor else return null (which stops it from being added to returned array)
      if (types[SUFFIX] !== undefined) {
        return anchor
      } else {
        return null
      }
    })

    return $newAnchors
  }


  /**
   * Set up GA downlaod tracking by binding the specified events in config.events to the each anchor element in $element
   * 
   * @param {Object} $elements - jQuery object of anchor elements
   * @param {Array} events - Array of javascript event descriptions and/or numeric key codes
   * @param {Object} defaultPayload - POJO of values to supply to the ga() function
   */
  bindTracking($elements, events, defaultPayload) {
    
    let elementsArray = $elements.toArray()

    // Loop through the array of matched anchor elements in $elements 
    for (let i = 0; i<elementsArray.length; i++) {

      

      // Use a regex to find the suffix/file name extension
      const MATCH = this.suffixRegex.exec(elementsArray[i].href)
      const SUFFIX = MATCH[1]

      // Create a new "payload" of info to send to Google Analytics
      let payload = new Object(defaultPayload)
      // Use the matched file extension to get it's file type category
      payload.action = this.types[SUFFIX]
      payload.label = elementsArray[i].href

      // Loop through our array of events and/or numeric key codes
      for (let x = 0; x<events.length; x++) {

        let eventString
        
        // Items in the config.events array can be numeric key codes in which case we need to use the "keydown" event descriptor otherwise we can just use the supplied event descriptor e.g. click
        if (typeof events[x] === "number") {
          eventString = "keydown"
        } else {
          eventString = events[x]
        }
        
        // Bind the reportInteraction function to the anchor element for the defined javascript event
        $(elementsArray[i]).on(eventString, (event) => {

          this.reportIntraction(event, payload)

        })
  
      }
    }

  }


  /**
   * Send the supplied payload to Google Analytics as an GA event
   * 
   * @param {Event} event - A javascript event
   * @param {Object} config.payload.category - GA eventCategory, typically the object that was interacted with e.g. Video
   * @param {Object} config.payload.action -  GA eventAction, the type of interaction (e.g. 'play')
   * @param {Object} config.payload.label - GA eventLabel, useful for categorizing events
   * 
   * Note: For some unknown reason we have used "File download" as the GA eventCategory and the file type as the GA eventAction
   */
  reportIntraction(event, payload) {

    let trackEvent = false

    // Is the event type descriptor (string) in our this.config.events array
    if (this.config.events.indexOf(event.type) !== -1) {

      trackEvent = true

    // If the event type descriptor is "keydown" we need to check which keycode was returned by the event
    } else if (event.type === "keydown") {
      // Is the key code in our this.config.events array
      if (this.config.events.indexOf(event.which) !== -1 || this.config.events.indexOf(event.keyCode) !== -1) {

        trackEvent = true

      }
    }

    if (trackEvent) {
      // Register an event with Google Analytics
      ga("send", "event", payload.category, payload.action, payload.label)

      // Diagnostic update
      this.notificationCentre.publish("diagnosticUpdate", {
        module: "GADownloadTracker",
        messages: [
          { text: `Tracked '${event.type}' event on`, variable: payload.label },
          { text: `GA category`, variable: payload.category },
          { text: `GA action`, variable: payload.action }
        ]
      })
    }

  }


  /**
   * Static function to instatiate the GADownloadTracker class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {ExampleClass} Reference to the same ExampleClass instatiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new GADownloadTracker(selector, config)
  }

}

export default GADownloadTracker