"use strict"

/**
 * Allows for basic cookie manipulation
 *
 * @since 1.1.2
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2017 State Government of NSW 2017
 *
 * @class
 */
class CookieControl {

  /**
   * @constructor
   */
  constructor() {
    this.debug = window.location.href.match("debug=true")

    // gets all the cookies
    this.allCookiesString = this.getCookies()

  }

  /**
   * @getCookies - returns the cookies
   *
   * @returns {Object}
   */
  getCookies() {
    return document.cookie
  }

  /**
   * @testName - tests the name as a string
   *
   * @param {string} name - name of the cookie to be modified
   *
   * @returns {true|false}
   */
  testName(name) {
    if (name === undefined || typeof name !== 'string' ) {
      console.error('CookieControl: you must pass a name (string) for the cookie')
      return false
    } else {
      return true
    }
  }

  /**
   * @returnCookie - returns the value of the cookie
   *
   * @param {string} name - name of the cookie to be returned
   *
   * @returns {this.cookieValue|false}
   */
  returnCookie(name) {
    // test the name param
    if (!this.testName(name)) {
      return false
    }

    let cookies = this.getCookies()
    // taken from http://stackoverflow.com/questions/10730362/get-cookie-by-name
    let value = "; " + cookies;
    let parts = value.split("; " + name + "=")
    if (parts.length == 2) {
      return parts.pop().split(";").shift()
    } else {
      this.cookieApplied = false
      return false
    }
  }

  /**
   * @setCookie - set the value and max_age of the cookie
   *
   * @param {string} name - name of the cookie to be returned
   * @param {string} value - the value of the cookie to be set
   * @param {string} max_age - the amount of time in seconds the cookie should remain in the user's browser
   *
   * @returns {true|false}
   */
  setCookie(name, value, max_age) {
    // test the name param
    if (!this.testName(name)) {
      return false
    }
        
    // set the cookie
    document.cookie = `${name}=${value};max-age=${max_age};path=/;samesite=strict${ this.debug ? "" : ";secure" }`
    this.cookieApplied = true
  }
}

/**
* Exports the Class Toggle class as a module
* @module
*/
export default CookieControl