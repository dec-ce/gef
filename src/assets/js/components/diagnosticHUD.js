"use strict"
import * as $ from "jquery"
import NotificationCentre from "core/notificationCentre"
import Environment from "core/environment"


/**
 * The DiagnosticHUD module displays diagnotic information broadcast via the NotificationCentre in a Heads Up Display (HUD)
 *
 * @since 1.1.15
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2016 State Government of NSW 2016
 *
 * @class
 * @requires jQuery
 * @requires NotificationCentre
 * @requires Environment
 */
class DiagnosticHUD {

  /**
   * Constructor for DiagnosticHUD
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options
   *
   * @example
   * // Instantiate a new DiagnosticHUD
   * let diagnosticHUD = new DiagnosticHUD(".gef-global-header", {})
   */
  constructor(selector, config) {

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    this.$container = $(selector)

    // Default class config options
    this.config = {}

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Get a reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()
    this.authenticated = false

    // Check to see we actually found the component in the DOM
    if (this.$container.length > 0) {
      
      this.$component = $(`<div class="gef-diagnostic-hud"></div>`)
      this.$container.prepend(this.$component)

      // Subscribe to diagnostic events broadcast by other classes
      this.notificationCentre.subscribe("diagnosticUpdate", this.updateHUD.bind(this))
    }
  }


  /**
   * Construct a new HTML update message
   */
  buildUpdate(payload) {

    let $update = $(`<div class="gef-diagnostic-hud__update">
      <span class="gef-diagnostic-hud__update-timestamp">${new Date().toLocaleTimeString()}</span>
      <span class="gef-diagnostic-hud__update-module">${payload.module}</span> 
    </div>`)

    for (let i = 0; i<payload.messages.length; i++) {
      
      let message = payload.messages[i]
      let textColour = "gef-diagnostic-hud__text-info"

      switch (message.variable) {
        case true:
          textColour = "gef-diagnostic-hud__text-success"
          break
        case false:
          textColour = "gef-diagnostic-hud__text-danger"
          break
        default:
          textColour = "gef-diagnostic-hud__text-info"
          break
      }

      $update.append($(`<span class="gef-diagnostic-hud__update-message">${message.text}${ message.hasOwnProperty("variable")? `: <span class="${textColour}">${message.variable}</span>`: ''}</span>`))
    }

    return $update
  }


  /**
   * Update the HUD
   */
  updateHUD(payload) {
    this.$component.prepend(this.buildUpdate(payload))
  }



  /**
   * Static function to instatiate the DiagnosticHUD class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {DiagnosticHUD} Reference to the same DiagnosticHUD instatiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new DiagnosticHUD(selector, config)
  }

}


/**
 * Exports the DiagnosticHUD class as a JS module
 * @module
 */
export default DiagnosticHUD
