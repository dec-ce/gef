"use strict"
import * as $ from "jquery"

/**
* dialogue.js - A popup modal which appears after a user interaction
*
* @since 0.2.02
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class Dialogue {

  /**
  * Creates a new Dialogue Scenario
  *
  * @constructor
  *
  * @param {String}   selector - the jQuery selector in question
  */

  constructor(selector, config)  {

    this.config = {
      $trigger    : $(selector)
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Set the template if it hasn't been passed via the config
    if (this.config.template.html === undefined) {

    }

    this.initialise()
  }

  /**
  * Something here
  *
  * @initialise
  *
  */
  initialise() {

  }
}

export default Dialogue
