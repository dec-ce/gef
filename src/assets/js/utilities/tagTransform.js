"use strict"
import * as $ from "jquery"

/**
 *
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */
class TagTransform {

  /**
   * Creates a new Tag Transform
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   * @param {String} config.selectors.active - class selector for the active state of toggle buttons
   *
   */
  constructor(selector, config) {

    // Default class config options
    this.config = {}

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Check if selector has been passed to constructor
    if (selector) {
      // Use jQuery to match find the relevant DOM element(s)
      this.$selector = $(selector)
      // News article tags
      this.$tags = this.$selector.data("gef-tags").split(";")
      this.$tagAmount = this.$tags.length
      // News article URL
      this.$newsIndex = this.$selector.data("news-index")
      // Empty UL
      this.$list = $("<ul class='gef-tag-list__list'></ul>")
      // Transform tags into link list
      this.tagTransform()
      // Add list to article
      this.$selector.replaceWith(this.$list)
    }

  }

  tagTransform() {
    //Split tags
    for (var i = 0; i < this.$tagAmount; i++) {
      var currentTag = this.$tags[i]
      currentTag = currentTag.charAt(0) == " " ? currentTag.substring(1) : currentTag
      var tagElement = "<li>&nbsp;<a href='" + this.$newsIndex + "?tag=" + currentTag + "' class='gef-smaller-text'>" + currentTag + "</a></li>"
      this.$list.append($(tagElement))
    }

    return this.$list
  }

}


/**
* Exports the Class Toggle class as a module
* @module
*/
export default TagTransform