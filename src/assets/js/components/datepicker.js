
"use strict"
import UI from "uikit!datepicker"
import * as $ from "jquery"

/**
* ui-kit-datepicker - UI Kit datepicker with accessibility enhancements.
*
* @class
* @requires jQuery
*/

class Datepicker {

  /* @param {string} selector - CSS selector or jQuery object of HTML element to use */

  constructor(selector, config) {

    this.config = {
      $selector: $(selector)
    }

    this.$selector = this.config.$selector
    this.initDatepicker()
  }

  initDatepicker(){
    // Define input element that the datepicker attaches to.
    var inputElement = this.$selector[0]
    // moveDatepicker function fires when input element is interacted with.
    this.$selector.on("keydown mousedown focus touchstart", this.moveDatepicker.bind(this))
  }

  moveDatepicker(){
    // Define element of the datepicker
    var targetElement = document.querySelectorAll('.uk-datepicker')
    // Moves datepicker element near the input field.
    this.$selector.after(targetElement[0]);
    // Retrieve the left position of the input field.
    var leftPosition = this.$selector.position().left
    //Finds the height of the input field and adds this to the top position.
    //This is so the datepicker sits at the bottom of input field.
    var inputHeight = this.$selector[0].offsetHeight
    var topPosition = this.$selector.position().top + inputHeight
    // Positions datepicker component
    setTimeout(function(){ 
      $('.uk-datepicker').css('top', topPosition)
      $('.uk-datepicker').css('left', leftPosition)
    }, 5);
    // Positioning for mobile
    if(window.innerWidth < 450){
      //mobile positioning
      var a = (window).innerWidth
      var b = $('.uk-datepicker').outerWidth()
      setTimeout(function(){ 
          $('.uk-datepicker').css('left', (a - b) / 2)
      }, 5);          
      //Clear placeholder
      this.$selector.attr('placeholder', '')
    } 
    // Hide datepicker if click outside the datepicker (this is needed for some tablets)
    $('body').bind('touchstart', function(e) {
      if($(e.target).closest('.uk-datepicker').length == 0 && $(e.target).closest('.gef-date-picker').length == 0) {
         $('.uk-datepicker').hide()
      }
    });
   //Call ARIAlabels function 
   this.ariaLabels()
  }

  ariaLabels(){
    //Add labels to the previous next button
    $('.uk-datepicker-previous').html('<span class="show-on-sr">Previous month</span>')
    $('.uk-datepicker-next').html('<span class="show-on-sr">Next month</span>')
    //This loop iterates over the anchors within every td and adds an aria label with the date
    $( ".uk-datepicker-table td a" ).each(function( index ) {
      var day = $(this).html(),
          months = ["January","February","March","April","May","June","July","August","September","October","November","December"],
          defaultDate = $(this).attr('data-date'),
          year = defaultDate.substring(0, 4),
          month = defaultDate.substring(5, 7)
      month = parseInt(month) - 1
      month = months[month]
      var ariadate = day + " " + month + " " + year
      $(this).attr('aria-label' , ariadate)
    });
    //Attach ARIA labels to days of the week.
    var counter = 0;
    $( ".uk-datepicker-table th" ).each(function( index ) {
      var daysOfWeek = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
      $(this).attr('aria-label', daysOfWeek[counter])
      counter++
    });
  }
}

export default Datepicker





