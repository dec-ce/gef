"use strict"
import * as $ from "jquery"

/**
 * anchorBox.js - Builds a set of anchors based on HTML content
 *
 * @since 0.1.16
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2016 State Government of NSW 2016
 *
 * @class
 * @requires jQuery
 */

class AnchorBox {

  /**
   * Creates a new AnchorBox
   *
   * @constructor
   *
   * @param {String}   selector - the jQuery selector in question
   * @param {Object}   config - class configuration options. Options vary depending on need
   * @param {String}   [config.$container = $(selector)] - the object to inject the AnchorBox pattern into
   * @param {String}   [config.targets = "h2"] - the selector to build the anchors from
   * @param {Integer}  [config.amount = 2] - amount of times selector needs to be found before the class ir
   * @param {String}   [config.template.html = undefined] - the html structure that will be used to build the AnchorBox
   * @param {String}   [config.template.selector = ".gef-anchor-box"] - the selector of the html structure
   * @param {String}   [config.template.list_class = "gef-link-list"] - the list pattern to inject into the AnchorBox
   * @param {String}   [config.inject_after_me = undefined] - if this exists then the AnchorBox will be placed after this element, must be a child of config.$container
   * @param {String}   [config.inject_after_me = undefined] - if this exists then the AnchorBox will be placed after this element, must be a child of config.$container
   */

  constructor(selector, parent, config) {

    this.config = {
      $container: $(selector),
      targets: "h3.gef-secondary-index-level1,p.gef-secondary-index-level1,h4.gef-secondary-index-level1",
      targetsl2: ".gef-secondary-index-level2",
      amount: 1,
      template: {
        html: undefined,
        selector: ".gef-secondary-anchor-box",
        list_class: "gef-secondary-link-list"
      },
      inject_after_me: "gef-secondary-index-parent",
      inject_backup: "gef-secondary-index-position"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Save the container for later
    this.$container = this.config.$container
    var containercheck = this.$container.attr("class");


    // Set the template if it hasn't been passed via the config
    if (this.config.template.html === undefined) {
      this.config.template.html = '<nav class="gef-secondary-anchor-box"></nav>'
    }

    this.initAnchorBox()
  }

  /**
   * Finds and tests the targets
   *
   * @initAnchorBox
   *
   */
  initAnchorBox() {

    // find the targets
    if (this.$container.attr("class").indexOf("gef-secondary-index-position") > -1) {
      this.$targets = $("body").find(this.config.targets)
    } else {
      this.$targets = this.$container.children(this.config.targets)
    }
    this.$targetsl2 = this.$targets.children(this.config.targetsl2)

    // this.$targets = this.$container.children(this.config.targets)
    // test to see if targets exists
    if (this.$targets.length < this.config.amount) {
      console.warn("Not enough targets to display AnchorBox. Needs " + this.config.amount + " '" + this.config.targets + "' selectors.")
      throw new RangeError("Couldn't find enough targets")
    } else {
      this.buildAnchorBox()
    }

  }

  /**
   * constructs the Anchor box
   *
   * @buildAnchorBox
   *
   */
  buildAnchorBox() {
    var component = this
    // add the anchor box
    if (this.config.inject_after_me !== undefined) {
      // var $injector = this.config.$container.children(this.config.inject_after_me),
      // $injector_backup = this.config.$container.children(this.config.inject_backup)
      if (this.$container.attr('class') == this.config.inject_after_me) {
        var $injector = this.$container
        // $injector_backup = this.config.$container.children(this.config.inject_backup)
      } else if ($("body").find("." + this.config.inject_backup)) {
        var $injector = $("." + this.config.inject_backup) //this.$container
      }
      if ($injector.length) {
        // add after specified element
        $injector.prepend(this.config.template.html)
      } else if (this.config.inject_backup !== undefined && $injector_backup.length) {
        // Use the backup inject plan
        $injector_backup.after(this.config.template.html)
      } else {
        // Can't place
        console.warn("Couldn't find where to place the AnchorBox")
      }
    } else {
      this.$container.prepend(this.config.template.html)
    }

    // create the link list
    if (this.config.template.selector)

    if (this.$container.attr('class') == this.config.inject_after_me) {
      this.$container.find(this.config.template.selector).append('<ul class="' + this.config.template.list_class + '"></ul>')
    } else {
      $("body").find(this.config.template.selector).append('<ul class="' + this.config.template.list_class + '"></ul>')
    }

    // loop through targets and add them to the box
    this.$targets.each(function (i) {
      // add an ID
      var htmlstructured = $(this).clone().text();
      htmlstructured = htmlstructured.trim()
      var anchor_name = htmlstructured.split(' ')[0] + i
      $(this).attr('id', anchor_name)
      // add anchors to the link list
      if (component.$container.attr('class') == component.config.inject_after_me) {
        component.$container.find('.' + component.config.template.list_class).append(
          '<li class="anchor-level' + [i] + '"><a href="#' + anchor_name + '">' + htmlstructured + '</a></li>'
        )
        var hasparent = "1"
      } else {
        $("body").find('.' + component.config.template.list_class).append(
          '<li class="anchor-level1"><a href="#' + anchor_name + '">' + htmlstructured + '</a></li>'
        )
        var hasparent = "0"
      }

      var check = $(this).nextUntil('.gef-secondary-index-level1', '.gef-secondary-index-level2').length;
      if (check > 0) {
        if (hasparent == "0") {
          component.$container.find($(".anchor-level1")[i]).append('<ul class="gef-secondary-link-list-child anchor-level-' + i + '"></ul>')
        } else {
          component.$container.find($(".anchor-level" + i)).append('<ul class="gef-secondary-link-list-child anchor-level-' + i + '"></ul>')
        }

        $(this).nextUntil('.gef-secondary-index-level1', '.gef-secondary-index-level2').each(function (j) {
          var htmlstructured1 = $(this).clone().text();
          var htmlstructured2 = htmlstructured1.substring(0, 100) 
          
          if (htmlstructured1.length > 100) {
            htmlstructured2 = htmlstructured2.substr(0, Math.min(htmlstructured2.length, htmlstructured2.lastIndexOf(" ")))
            htmlstructured2= htmlstructured2.replace(/\s*$/,"")+`…<span class="show-on-sr">link text truncated</span>`
           }
          var anchor_name1 = htmlstructured1.split(' ')[0] + i
          $(this).attr('id', anchor_name1)
          if (component.$container.attr('class') == component.config.inject_after_me) {
            component.$container.find($('.anchor-level-' + i + '')).append(
              '<li class="anchor-level3"><a href="#' + anchor_name1 + '">' + htmlstructured2 + '</a></li>'
            )
          } else {
            $("body").find($('.anchor-level-' + i + '')).append(
              '<li class="anchor-level3"><a href="#' + anchor_name1 + '">' + htmlstructured2 + '</a></li>'
            )
          }
        })
      }

    })
  }
}



export default AnchorBox
