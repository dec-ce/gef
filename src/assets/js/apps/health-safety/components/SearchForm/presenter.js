import React from "react"
import ReactAutocomplete from "reactAutocomplete"
import _ from "lodash"

const SearchForm = (props) => {
    const providers = props.providers;
    const schools = props.schools;
    const searchinput = props.searchinput;
    const invalidschool = props.invalidschool;
    const onSetSearchInput = props.onSetSearchInput;
    const onSetSelectedSchool = props.onSetSelectedSchool;

    const handleSearch = (event) => {
        event.preventDefault();
        const selectedSchool = _.find(schools, {school: searchinput});
        onSetSelectedSchool(selectedSchool, providers);
    };

    if (!schools) {
        return "";
    } else {
        return (
            <form id="frmschool" name="frmschool" className="gef-search uk-form gef-search--body" onSubmit={handleSearch}>
                <h2>Enter your school name to view approved providers</h2>
                <fieldset>
                    <legend className="show-on-sr">Search the approved panel of registered training providers</legend>
                    <label htmlFor="searchrrto">
                        <span className="show-on-sr">Enter your school name</span>
                    </label>
                    <ReactAutocomplete
                        inputProps={{
                            id: "searchrrto",
                            className: "ui ui-autocomplete-input",
                            maxLength: 254,
                            type: "text",
                            "aria-haspopup": true
                        }}
                        items={_.toArray(schools)}
                        shouldItemRender={(item, value) => value.length >= 3 && item.school.toLowerCase().indexOf(value.toLowerCase()) > -1}
                        getItemValue={item => item.school}
                        renderItem={(item, highlighted) =>
                            <div key={item.code}
                                 style={{backgroundColor: highlighted ? "#eee" : "transparent"}}>
                                {item.school}
                            </div>
                        }

                        value={searchinput === null ? "" : searchinput}
                        onChange={element => onSetSearchInput(element.target.value)}
                        onSelect={value => onSetSearchInput(value)}
                    />
                    <button type={"button"} id={"submit_schoolsearch"} aria-label={"Submit search"} onClick={handleSearch}>
                        SEARCH
                    </button>
                </fieldset>
                {
                    invalidschool === null ? "" : <div style={{display: "block"}} id={"no_schoolname"}>
                        <p><span className="rrtoerror">{invalidschool}</span></p>
                    </div>
                }
            </form>
        );
    }
};

export default SearchForm;