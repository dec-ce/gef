import React from "react"
import SearchForm from "apps/bushfire-register/components/SearchForm/index"
import BushfireProne from "apps/bushfire-register/components/BushfireProne/index"
import SearchRegionForm from "apps/bushfire-register/components/SearchRegionForm/index"
import RegionBushfireProne from "apps/bushfire-register/components/RegionBushfireProne/index"

const App = () => [
    <p key={"1"}>Please search for your school or site on the department"s bush fire register below:</p>,
    <SearchForm key={"2"}/>,
    <BushfireProne key={"3"}/>,
    <p key={"4"}><strong>Search for a NSW public school or site by NSW fire area</strong></p>,
    <SearchRegionForm key={"5"}/>,
    <RegionBushfireProne key={"6"}/>
]

export default App
