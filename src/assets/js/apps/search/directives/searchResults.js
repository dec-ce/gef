"use strict"
import directives from "apps/search/directives/directives"

directives.directive("sspaSearchResults", ["configService", "searchService", function(Config, searchService) {
  return {
    restrict: "AE",
    replace: true,
    templateUrl: Config.views.searchResults,
    scope: true
  }
}])
