import React from "react"

const Providers = (props) => {
    const selectedschool = props.selectedschool;
    const selectedproviders = props.selectedproviders;

    if (!selectedschool || !selectedproviders) {
        return "";
    } else {
        return (
            <div id={"results"} style={ { display: "block" } }>
                <h2 id={"school"}>{selectedschool.school}</h2>
                <p>*Prices shown below are indicative only and exclusive of any travel costs. The maximum 'price per
                    person' cost provided is the ceiling price (inclusive of GST) the provider is able to charge. The
                    price per person cost may vary depending on the amount of people attending a session e.g. 2 people
                    per session may be significantly more expensive than 20 people per session. It is recommended that
                    each school contact at least 3 providers and confirm the 'price per person' cost for the requested
                    training session.</p>
                <table id={"data_table"}>
                    <thead>
                    <tr>
                        <th>Provider Name</th>
                        <th>No. of Trainers</th>
                        <th className={"preferredsel"}>Combined CPR &amp; Anaphylaxis*</th>
                        <th>Provider CPR*</th>
                        <th>Anaphylaxis*</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        selectedproviders.map((provider, key) => {
                            return (
                                <tr key={key}>
                                    <td>
                                        <strong>{provider.name}</strong>
                                        <br/>{provider.contact}
                                        <br/>{provider.phone}
                                        <br/><a href={provider.email}>{provider.email}</a>
                                    </td>
                                    <td>{provider.trainers}</td>
                                    <td className={"preferredsel"}>{provider.combined}</td>
                                    <td>{provider.cpr}</td>
                                    <td>{provider.anaphylaxis}</td>
                                </tr>
                            );
                        })
                    }
                    </tbody>
                </table>
                <div id="totalprov"><p>Total Providers: <span id={"num_providers"}>{selectedproviders.length}</span></p>
                </div>
            </div>
        );
    }
};

export default Providers;
