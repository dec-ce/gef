"use strict"
import * as $ from "jquery"

class BackToTop {

  // @constructor
  // @param {string} selector - CSS selector or jQuery object of HTML element to use
  // @param {object} config   - class configuration arguments
  //   @param {object} config.selector - CSS selectors to use in conjunction with class selector
  //     @param {string} config.selectors.footer  - Footer CSS selector
  //     @param {string} config.selectors.main    - Main content element CSS selector
  //     @param {string} config.selectors.sideNav - Side navigation CSS selector
  //     @param {string} config.selectors.ie9     - Global ie9 CSS class
  //   @param {object} config.modifier - CSS classes for state/behavioural changes
  //     @param {string} config.modifier.active   - active CSS class
  //     @param {string} config.modifier.fixed    - fixed CSS class
  //   @param {string} config.width    - Width for media query hook
  //

  constructor(selector, config) {

    this.config = {
      selectors: {
        footer: ".gef-section-footer",
        main: ".gef-main-content",
        sideNav: ".gef-side-navigation",
        ie9: ".gef-ie9"
      },
      modifier: {
        active: "active",
        fixed: "fixed"
      },
      width: "960px",
      clearance: 120
    }

    this.$selector = $(selector)
    // Merge default config with passed config
    $.extend(this.config, config)

    // Check if Back-to-Top button was found by jQuery
    if (this.$selector.length === 0) {
      throw `Failed to instantiate the BackToTop class because an element with a CSS selector of "#{selector}" could not be found`
    }


    this.$window = $(window)
    this.height = this.$window.height()

    if (window.matchMedia) {
      this.mql = window.matchMedia(`screen and (min-width: ${this.config.width})`)
    }

    // Remove the button when the content is smaller than the screen
    this.testContentHeight()

    this.$window.on("scroll", this.reposition.bind(this) )
  }


  // Check position and height of the viewport and either show or hide the back to top button
  reposition() {
    var top = this.$window.scrollTop(),
    bottom  = this.$footer.offset().top - this.height + 20,
    trigger

    if (window.matchMedia) {

      if (!this.mql.matches) { // only relevant on mobile

        // Longer than 0.5 times the screen height
        trigger = this.height * 0.5

      } else {

        // Longer than the main region is taller than the menu + set clearance
        trigger = parseInt($(this.config.selectors.sideNav).height() + this.config.clearance)
      }

    } else {
      // IE9
      trigger = parseInt($(this.config.selectors.sideNav).height() + this.config.clearance)
    }

    if (top > trigger) {

      if (top < bottom) {
      //Change the button position to absolute keep shown
      this.show()
      this.attach()

      } else {
        //Change the button position to fixed and show
        this.show()
        this.detach()
      }

    } else {
      //Change the button position to absolute and hide
      this.hide()
    }
  }

  // Attached to bottom of viewport
  attach() {
    return this.$selector.addClass(this.config.modifier.fixed)
  }

  // Detach from bottom of viewport
  detach() {
    return this.$selector.removeClass(this.config.modifier.fixed)
  }

  // Show button
  show() {
    return this.$selector.addClass(this.config.modifier.active)
  }

  // Hide button
  hide() {
    return this.$selector.removeClass(this.config.modifier.active)
  }

  // Test to see whether the height of the content warrants a back to top button
  testContentHeight() {

    this.$footer = $(this.config.selectors.footer)

    if (this.height > this.$footer.offset().top) {
      return this.$selector.remove()
    }
  }

  // Remove button
  removeFromDOM() {
    return this.$selector.remove()
  }

  // Static function to instatiate class as singleton
  //
  // @param {string|object} selector - CSS selector of back to top button
  // @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
  static shared(selector, config) {
    this.instance != null ? this.instance : this.instance = new BackToTop(selector, config)
    return this.instance
  }

}

/**
 * BackToTop button manager module
 * @module components/BackToTop
 */
export default BackToTop
