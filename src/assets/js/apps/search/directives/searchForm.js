"use strict"
import directives from "apps/search/directives/directives"

directives.directive("sspaSearchForm", ["configService", "searchService", function(Config, searchService) {
  return {
    restrict: "AE",
    replace: true,
    templateUrl: Config.views.searchForm,
    link: function(scope, element, attributes) {

      scope.keyword = searchService.searchTerm
      scope.searchTerm = searchService.searchTerm

      scope.newSearchTerm = function() {
        scope.searchTerm = scope.keyword
        searchService.pageNumber = 1
        searchService.getResults(scope.keyword, 1)
      }

    }
  }
}])
