import {SELECTEDSCHOOLS_SET} from "apps/bushfire-register/constants/actionTypes"
import _ from "lodash"

export const setSelectedSchools = (searchinput, bushfireprone) => {
    const selectedschools = _.reduce(bushfireprone, function(result, value) {
        if (value.name.toLowerCase().indexOf(searchinput.toLowerCase()) > -1) {
            return _.concat(result, value)
        } else {
            return result
        }
    }, [])

    if (!selectedschools || selectedschools.length === 0) {
        return dispatch => dispatch({
            type: SELECTEDSCHOOLS_SET,
            selectedschools: [{name: searchinput, bushfireProne: false, hishRisk: false}]
        })
    } else {
        return dispatch => dispatch({
            type: SELECTEDSCHOOLS_SET,
            selectedschools
        })
    }
}