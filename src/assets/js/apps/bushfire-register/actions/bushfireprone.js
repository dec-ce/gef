import axios from "axios"
import {
    BUSHFIREPRONE_GET_FULFILLED,
    BUSHFIREPRONE_GET_REJECTED,
    BUSHFIREPRONE_GET_REQUESTED,
    REGIONS_SET,
    SCHOOLS_SET
} from "../constants/actionTypes"
import _ from "lodash"

export function getBushfireProne() {
    return dispatch => {
        dispatch(getBushfireProneRequestedAction())

        axios.get(
            "https://doe-health-and-safety.firebaseio.com/bushfire.json"
        ).then(response => {
            const bushfireprone = response.data
            dispatch(getBushfireProneFulfilledAction(bushfireprone))
            dispatch(getRegions(bushfireprone))
            dispatch(getSchools(bushfireprone))
        }).catch((error) => {
            console.log(error)
            dispatch(getBushfireProneRejectedAction())
        })
    }
}

function getRegions(bushfireprone) {
    const regionsfull = _.transform(bushfireprone, (result, value) => {
        result.push(value.nswFireArea)
        return true
    }, [])

    const sortedregionsfull = _.sortBy(regionsfull, o => {return o})
    const regions = _.sortedUniq(sortedregionsfull)

    return {
        type: REGIONS_SET,
        regions
    }
}

function getSchools(bushfireprone) {
    const unsortedschools = _.transform(bushfireprone, (result, value) => {
        result.push(value.name)
        return true
    }, [])

    const schools = _.sortBy(unsortedschools, o => {return o})
    return {
        type: SCHOOLS_SET,
        schools
    }
}

function getBushfireProneRequestedAction() {
    return {
        type: BUSHFIREPRONE_GET_REQUESTED
    }
}

function getBushfireProneFulfilledAction(bushfireprone) {
    return {
        type: BUSHFIREPRONE_GET_FULFILLED,
        bushfireprone
    }
}

function getBushfireProneRejectedAction() {
    return {
        type: BUSHFIREPRONE_GET_REJECTED
    }
}
