import * as actionTypes from "apps/health-safety/constants/actionTypes"

const initialState = null;

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SELECTEDSCHOOL_SET:
            return setInvalidSchool(state, action);
        default:
            return state;
    }
}

function setInvalidSchool(state, action) {
    const { selectedschool } = action;
    if (!selectedschool) {
        return "Please enter a valid school name";
    } else {
        return null;
    }
}