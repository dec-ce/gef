import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.BUSHFIREPRONE_GET_FULFILLED:
            return setBushfireProne(state, action)
        default:
            return state
    }
}

function setBushfireProne(state, action) {
    const { bushfireprone } = action
    return bushfireprone
}