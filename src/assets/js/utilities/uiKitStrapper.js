"use strict"
import UI from "uikit"
import * as $ from "jquery"

/**
 * Create a new UIKitStrapper facade which depending on mehtod of initialisation will return a jQuery object
 *
 * @example
 * // instantiate using class constructor
 * let switcher = new UIKitStrapper("[data-gef-uk-switcher]", { ukComponent: "switcher", optionsAttr: "data-gef-uk-switcher" })
 * @example
 * // instantiate using instance method
 * let strapper = new UIKitStrapper()
 * let switcher = strapper.init("[data-gef-uk-switcher]", { ukComponent: "switcher", optionsAttr: "data-gef-uk-switcher" })
 * @example
 * // instantiate using static method
 * let switcher = UIKitStrapper.init("[data-gef-uk-switcher]", { ukComponent: "switcher", optionsAttr: "data-gef-uk-switcher" })
 *
 * @class
 */
class UIKitStrapper {

  /**
   * @param {(String|jQuery)} selector - A CSS selector or jQuery object
   * @param {Object} config - Object literal
   * @param {string} config.ukComponent - The name of a UIKit Core component you want to intitialise
   * @param {string} config.optionsAttr - The atrribute containing the UIKit options corresponding to the UIKit Core component
   *
   * @returns {UIKitStrapper|jQuery} Returns a UIKitStrapper object or a jQuery object (the result of a sucessful initialisation of a UIKit Core component)
   */
  constructor(selector, config) {
    // If the selector and ukComponent option has been provided to the constructor then initialise the UIKit core component
    if (selector && config.ukComponent) {
      return this.init(selector, config)
    }
  }

  /**
   * Instance method for initialising a UIKit Core component
   * @see {@link constructor}
   */
  init(selector, config) {
    return UIKitStrapper.init(selector, config)
  }

  /**
   * Static method for initialising a UIKit Core component. See constructor documentation for details on the paramaters
   * @see {@link constructor}
   * @returns {jQuery|false} Returns a jQuery object (the result of a sucessful initialisation of a UIKit Core component) otherwise false will be returned if the element is not found on the page or the UIKit component hasn't been specified
   */
  static init(selector, config) {
    let ukOptions = null
    let $element = $(selector)

    // Check if jQuery has found the element in the DOM and that the name of a UIKit core component has been provided
    if ($element.length === 0 || config.ukComponent === null) {
      return false
    }

    // If the component has options to pass
    if (config.optionsAttr) {
      // Use UIKit's "options" utility to convert the JSON object contained in the HTML attribute into a JavaScript object literal
      ukOptions = UI.Utils.options($element.attr(config.optionsAttr))
    }

    return $element.uk(config.ukComponent, ukOptions)
  }

}

  /**
   * A module for initialising UIKit's "Core" javascript compontents.
   * It returns a UIKitStrapper facade. The module is required because
   * of a bug in UIKit where it doesn't instantiate (via the data-uk-*
   * selectors) UIKit Core components correctly when UIKit is loaded
   * via RequireJS
   *
   * @module utilities/uiKitStrapper
   * @requires UIKit
   * @requires jQuery
   */
   export default UIKitStrapper
