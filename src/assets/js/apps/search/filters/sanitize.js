"use strict"
import filters from "apps/search/filters/filters"

// necessary to display HTML as intended, not the code source.
filters.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode)
  }
}])
