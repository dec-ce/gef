"use strict"
import Observable from "core/observable"

/**
 * NotificationCentre is a singleton that can be used to broadcasting information across an app
 *
 * @version 1.0
 * @author NSW Department of Education and Communities
 *
 * @class
 * @extends Observable
 */
 class NotificationCentre extends Observable {

  /**
   * Returns a pointer to a shared instantiated NotificationCentre object i.e. a singleton
   *
   * @returns {NotificationCentre} NotificationCentre object
   */
  static shared() {
    this.instance != null ? this.instance : this.instance = new NotificationCentre()
    return this.instance
  }
}

export default NotificationCentre
