import _ from "lodash"
import {REGIONSCHOOLS_SET} from "apps/bushfire-register/constants/actionTypes"

export function getBushfireProneRegion(selectedregion, bushfireprone) {
    return dispatch => {
        const unsortedregionschools = _.transform(bushfireprone, (result, value) => {
            if (value.nswFireArea === selectedregion && (value.bushfireProne || value.hishRisk)) {
                result.push(value)
                return true
            }
        }, [])

        const regionschools = _.sortBy(unsortedregionschools, (regionschool) => {return regionschool.name})
        dispatch(setRegionSchools(regionschools))
    }
}

function setRegionSchools(regionschools) {
    return {
        type: REGIONSCHOOLS_SET,
        regionschools
    }
}
