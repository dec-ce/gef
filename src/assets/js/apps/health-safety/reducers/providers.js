import {PROVIDERS_GET_FULFILLED} from "apps/health-safety/constants/actionTypes"

const initialState = [];

export default function(state = initialState, action) {
    switch (action.type) {
        case PROVIDERS_GET_FULFILLED:
            return setProviders(state, action);
        default:
            return state;
    }
}

function setProviders(state, action) {
    const { providers } = action;
    return providers;
}