import {connect} from "reactRedux"
import lifecycle from "reactPureLifecycle"
import SearchForm from "apps/bushfire-register/components/SearchForm/presenter"
import {setSearchInput} from "apps/bushfire-register/actions/searchinput"
import {setSelectedSchools} from "apps/bushfire-register/actions/selectedschools"
import {getBushfireProne} from "apps/bushfire-register/actions/bushfireprone"

function mapStateToProps(state) {
    const schools = state.schools
    const searchinput = state.searchinput
    const bushfireprone = state.bushfireprone
    return {
        schools,
        searchinput,
        bushfireprone
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetBushfireProne: () => dispatch(getBushfireProne()),
        onSetSearchInput: (searchinput) => dispatch(setSearchInput(searchinput)),
        onSetSelectedSchools: (searchinput, bushfireprone) => {
            dispatch(setSearchInput(searchinput))
            dispatch(setSelectedSchools(searchinput, bushfireprone))
        }
    }
}

const componentWillMount = (store) => {
    store.onGetBushfireProne()
    store.onSetSearchInput("")
}

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(SearchForm)

export default connect(mapStateToProps, mapDispatchToProps)(cwm)