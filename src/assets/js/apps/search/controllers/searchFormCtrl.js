"use strict"
import controllers from "apps/search/controllers/controllers"

// searchCtrl
// Linked to the input form for new keywords
// just listens for the button to be clicked and refreshes the results when found

controllers.controller("SearchFormCtrl", ["$scope", "searchService", function($scope, searchService) {

  $scope.keyword = searchService.searchTerm
  $scope.searchTerm = searchService.searchTerm
  $scope.siteSearch = searchService.siteSearch
  $scope.searchSiteName = searchService.searchSiteName
  $scope.previousPage = searchService.previousPage

  // check to see if the button is clicked
  $scope.newSearchTerm = function() {
    $scope.searchTerm = $scope.keyword
    searchService.pageNumber = 1
    searchService.newPage = 1
    searchService.getResults($scope.keyword, 1)
  }

}])
