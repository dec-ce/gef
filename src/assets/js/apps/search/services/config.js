"use strict"
import services from "apps/search/services/services"

services.factory("configService", function() {

  return {
    url: {
      base: jQuery("head").data("siteBase") ? jQuery("head").data("siteBase") : "https://online.det.nsw.edu.au/gsa-jsonp-bridge/gsasearch.json",
      params: {
        frontend: "jsonp",
        collection: jQuery("head").data("siteCollection") ? jQuery("head").data("siteCollection") : "public_ce_dec_nsw_gov_au",
        callback: "JSON_CALLBACK",
        query: null,
        start: null,
        num: 1
      }
    },
    siteName: "NSW Department of Education",
    resultsPerPage: 10,
    views: {
      searchForm: "assets/js/apps/search/views/searchForm.html",
      searchResults: "assets/js/apps/search/views/searchResults.html"
    },
    reportError: {
      url: "https://cms.det.nsw.edu.au/iaf/search-error-found"
    }
  }

})
