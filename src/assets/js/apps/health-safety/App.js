import React from "react"
import SearchForm from "apps/health-safety/components/SearchForm/index"
import Providers from "apps/health-safety/components/Providers/index"

const App = () => [
    <SearchForm key={"1"}/>,
    <Providers key={"2"}/>
];

export default App
