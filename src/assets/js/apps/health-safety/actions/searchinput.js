import { SEARCHINPUT_SET } from "apps/health-safety/constants/actionTypes"

export function setSearchInput(searchinput) {
    return dispatch => {
        dispatch({
            type: SEARCHINPUT_SET,
            searchinput
        });
    }
}
