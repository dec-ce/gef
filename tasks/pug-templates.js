"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {

  return function() {

    return gulp.src([options.paths.dev.index, options.paths.dev.templates])

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // use gulp-cached to only run pug on templates that have changed.
      .pipe(plugins.cached('pug-templates'))

      // Pug
      .pipe(plugins.pug({
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/"
        }
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.root))

  }
}
