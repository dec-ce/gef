"use strict"

var configuration = {
  name : "GEF Examples",
  components : {
    "scrollToggle" : {
      "selector" : "[data-gef-eg-scrolltoggle]",
      "script" : "utilities/scrollToggle",
      "config" : {
        "checkpoint" : 500,
        "passed_attr" : "data-changed"
      }
    },

    "scrollToggleAgainOnSameElement" : {
      "selector" : "[data-gef-eg-scrolltoggle]",
      "script" : "utilities/scrollToggle",
      "config" : {
        "passed_attr" : "data-changed-again"
      }
    },

    "drawerAccessibility" : {
      "selector" : ".gef-drawer",
      "script" : "utilities/ariaToggler",
      "config" : {
        "click_outside" : true,
        "tabindex_toggle" : false
      }
    },

    "anchorScroll" : {
      "selector" : "body",
      "script" : "utilities/scrollToggle",
      "config" : {
        "checkpoint" : "[data-gef-a-z-anchors-scroll]",
        "subject"     : ".ac",
        "passed_attr" : "data-scroll-passed",
        "scroll_window" : true,
        "scroller_position" : "top"
      }
    },

    "AnchorHighlight" : {
      "selector" : ".gef-a-z-anchors",
      "script" : "components/anchorHighlight",
      "singleton" : false
    },

    "RefineSearchResults" : {
      "selector" : "#refine-search-form",
      "script" : "utilities/submitOnEvent",
      "config" : {      
        "selector"    : ".refine-search-results",
        "triggers"     : "mouseup touchend",
        "submitForm"   : "#refine-search-form",
        test         : false
      }
    }, 

    
    "drawerToggle" : {
      "selector" : ".gef-drawer a",
      "script" : "utilities/classManipulator",
      "config" : {
        "event_type"  : "click",
        "subject"     : ".gef-drawer",
        "toggle"      : true,
        "click_outside" : true,
        "click_outside_ignores" : ".gef-drawer div"
      }
    },
  },
  requireConfig : {

  }
}

// Update RequireJS config options
require.config(configuration.requireConfig)

export default configuration