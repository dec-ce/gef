import {connect} from "reactRedux"
import lifecycle from "reactPureLifecycle"
import SearchForm from "apps/health-safety/components/SearchForm/presenter"
import {getSchools} from "apps/health-safety/actions/schools"
import {setSearchInput} from "apps/health-safety/actions/searchinput"
import {setSelectedSchool} from "apps/health-safety/actions/selectedschool"

function mapStateToProps(state) {
    const providers = state.providers;
    const schools = state.schools;
    const searchinput = state.searchinput;
    const invalidschool = state.invalidschool;
    return {
        providers,
        schools,
        searchinput,
        invalidschool
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetSchools: () => dispatch(getSchools()),
        onSetSearchInput: (searchinput) => dispatch(setSearchInput(searchinput)),
        onSetSelectedSchool: (selectedschool, providers) => setSelectedSchool(dispatch, selectedschool, providers),
    }
}

const componentWillMount = (store) => {
    store.onGetSchools();
    store.onSetSearchInput("");
};

const cwm = lifecycle({
    componentWillMount: componentWillMount
})(SearchForm);

export default connect(mapStateToProps, mapDispatchToProps)(cwm);
