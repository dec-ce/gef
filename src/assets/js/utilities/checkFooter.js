"use strict"
import * as $ from "jquery"

/**
  * Loops through A to Z list and makes unused tabs inactive.
  *
  * @since 0.4.0
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2015 State Government of NSW 2015
  *
  * @class
  * @requires jQuery
  */

class CheckFooter {

  constructor(selector, config)  {

  /**
    *
    * @param {string} config.container: selector for the A to Z ordered list.
    * @param {string} config.inactive_class: The class that gets added to the tabs that are missings hrefs.
    *
    */

    this.config = {
      "container": selector,
      "inactive_class": "inactive"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Find the "li" in footer class and store in variable
    this.contactLinks = $(this.config.container).find('li')
    // Find if social media is in footer class and store in variable
    this.socialmediaLinks = $(this.config.container).find('a')
   // console.log(this.config)   

    // Hide the footer if no footer links or social media link exists
   if ((this.contactLinks.length == 0) && (this.socialmediaLinks.length == 0)) {
            
            $(this.config.container).empty();
            $(this.config.container).css({'background-color': "#fff",'padding': '0',});
            //$(this.config.container).removeClass('gef-section-footer');
            //$(this.config.container).append('<div class="gef-section-footer" style="background-color:#fff;padding:0;"></div>')
      }

  }

}
export default CheckFooter

