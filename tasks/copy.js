"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // jQuery
    gulp.src(options.paths.bower + "jquery/dist/" + (options.compress ? "jquery.min.js" : "jquery.js"))
      .pipe(plugins.rename("jquery.js"))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor"))
    // UIKit
    var uikitPaths
    if (options.compress) {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.min.js"]
    } else {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.js", "!" + options.paths.bower + "uikit/js/**/*.min.js"]
    }

    gulp.src(uikitPaths)
      .pipe(plugins.rename(function(path) {
        path.basename = path.basename.replace(".min", "")
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/uikit"))

    // Font Awesome
    gulp.src(options.paths.bower + "uikit/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths.build.assets.fonts))

    // Text Generator pug
    gulp.src([options.paths.bower + "text-generator-pug/lorem-ipsum-mixin.pug", options.paths.bower + "text-generator-pug/generate-text.pug"])
      .pipe(gulp.dest(options.paths.dev.assets.root + "pug/utilities/"))

    // RequireJS
    gulp.src(options.paths.bower + "requirejs/require.js")
      .pipe(plugins.if(options.compress, plugins.uglify()))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor"))

    // Require CSS RequireJS plugin
    gulp.src(options.paths.bower + "require-css/" + (options.compress ? "css.min.js" : "css.js"))
      .pipe(plugins.rename("css.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/requirejs-plugins/css"))

    // Config for Require
    gulp.src(options.paths.dev.assets.jsonConfig + "config.json")
      .pipe(gulp.dest(options.paths.build.assets.js))

    // RequireJS Text & JSON Plugins (JSON requires Text)
    gulp.src([options.paths.bower + "requirejs-plugins/src/json.js", options.paths.bower + "requirejs-text/text.js"])
      .pipe(plugins.if(options.compress, plugins.uglify()))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/requirejs-plugins/js"))

    // Placeholder support for IE9
    gulp.src(options.paths.bower + "jquery-placeholder/jquery.placeholder.js")
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/jquery-placeholder"))

    // Angular for Search SPA
    gulp.src(options.paths.bower + "angular/" + (options.compress ? "angular.min.js" : "angular.js"))
      .pipe(plugins.rename("angular.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/angular"))

    gulp.src(options.paths.bower + "angular-sanitize/" + (options.compress ? "angular-sanitize.min.js" : "angular-sanitize.js"))
      .pipe(plugins.rename("angular-sanitize.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/angular-sanitize"))

    gulp.src(options.paths.bower + "angular-utils-pagination/dirPagination.js")
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/angular-utils-pagination"))

    gulp.src(options.paths.bower + "jquery-accessible-tabpanel-aria/jquery-accessible-tabs.js")
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/jquery-accessible-tabpanel-aria"))

   // Jquery form validation

    gulp.src(options.paths.bower + "jquery-validation/dist/" + (options.compress ? "jquery.validate.min.js" : "jquery.validate.js"))
      .pipe(plugins.rename("validate.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/jquery-validation"))
      

    // Momment https://momentjs.com
    gulp.src(options.paths.bower + "moment/min/moment.min.js")
    .pipe(plugins.rename("moment.js"))
    .pipe(gulp.dest(options.paths.build.assets.js + "vendor/moment"))


    // doT.js http://olado.github.io/doT/index.html
    gulp.src(options.paths.bower + "dot/" + (options.compress ? "doT.min.js" : "doT.js"))
    .pipe(plugins.rename("doT.js"))
    .pipe(gulp.dest(options.paths.build.assets.js + "vendor/dot"))


    // Object.assign polyfill
    gulp.src(options.paths.nodeModules + "es6-object-assign/dist/" + (options.compress ? "object-assign-auto.min.js" : "object-assign-auto.js"))
      .pipe(plugins.rename("object-assign-auto.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/rubennorte"))


    // annyang voice recognititon
    return gulp.src(options.paths.bower + "annyang/" + (options.compress ? "annyang.min.js" : "annyang.js"))
      .pipe(plugins.rename("annyang.js"))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/annyang"))

  }
}
