"use strict"
import controllers from "apps/search/controllers/controllers"

// ShowResultsCtrl
// displays results of search found in the $scope.resultsPerPage
// results and nav are variables expected by the pagination control
// other variable expected at scope level is the resultsPerPage field.

controllers.controller("ShowResultsCtrl", ["$scope", "searchService", "configService", function($scope, searchService, configService) {
  // This takes the information from the result pages and displays them using the pagination extensions.

  $scope.resultsPerPage = configService.resultsPerPage
  $scope.results = searchService.results
  $scope.navigation = searchService.navigation
  $scope.pageNumber = searchService.pageNumber
  $scope.searchTerm = searchService.searchTerm
  $scope.searchSite = searchService.searchSite
  $scope.searchSiteChosen = searchService.searchSiteChosen
  $scope.searchUrl = searchService.searchUrl
  $scope.siteLevels = searchService.siteLevels
  $scope.hasError = searchService.error
  $scope.searchSiteName = searchService.searchSiteName
  $scope.setSearchUrl = "education.nsw.gov.au"
  $scope.previousPage = searchService.previousPage

  // Listens for the ajax function on the controller to finish, and when is, updates the displayed results
  // Ajax function returns that the queryTerm has updated.
  $scope.$watch(function() { return searchService.queryTerm }, function(newValue, oldValue) {
    $scope.navigation = searchService.navigation
    $scope.results = searchService.results
    $scope.searchTerm = searchService.searchTerm
    $scope.searchSite = searchService.searchSite
    $scope.siteLevels = searchService.siteLevels
    $scope.pageNumber = searchService.pageNumber
    $scope.currentPage = searchService.currentPage
    $scope.searchSiteChosen = searchService.searchSiteChosen
    $scope.searchUrl = searchService.searchUrl
    $scope.browserAgent = searchService.browserAgent
    $scope.searchSiteName = searchService.searchSiteName
    $scope.previousPage = searchService.previousPage
    if (searchService.error) { // when there is an error, call the register error function
      searchService.registerError($scope.searchTerm, $scope.pageNumber, $scope.currentPage, $scope.browserAgent)
    }
    $scope.hasError = searchService.error

   }, true)

   // listens for a change of pageNumber and initiates the query for that pages
   $scope.$watch("pageNumber", function(newValue, oldValue) {
     searchService.getResults(searchService.searchTerm, $scope.pageNumber, $scope.searchSiteChosen)
   })

   // pagination listener for new page click
   $scope.pageChanged = function(newPage) {
     searchService.pageNumber = newPage
     $scope.pageNumber = newPage
     $scope.previousPage = newPage
     searchService.previousPage = newPage
   }


   $scope.newSearchScope = function(url, title, thisId) {
     url = url.replace(/\/$/, "")
     searchService.pageNumber = 1
     searchService.searchSiteChosen = url
     $scope.searchSiteName = searchService.searchSiteName = title
     searchService.getResults($scope.searchTerm, 1, url, title)
   }
}])
