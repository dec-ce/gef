"use strict"
import * as $ from "jquery"

/**
 * A class toggling script designed for view changes only. Do not use for showing and hiding content.
 * Instead use AriaToggler and apply CSS on the attribute changes.
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */
class ClassToggle {

  /**
   * Creates a new Class Toggle
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   * @param {String} config.selectors.active - class selector for the active state of toggle buttons
   *
   */
  constructor(selector, config) {

    // Default class config options
    this.config = {
      selectors : {
        active : "uk-active"
      }
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
      console.log(this.config, config)
    }

    // Check if selector has been passed to constructor
    if (selector) {
      // Use jQuery to match find the relevant DOM element(s)
      this.$selector = $(selector)
      // Class to toggle
      this.toggleClass = this.$selector.data("gef-class-toggle")
      // Element to toggle class on
      this.toggleTarget = this.$selector.data("gef-class-toggle-target")
      // Initiate event listener for click of button
      this.$selector.on("click", this.clickLogic.bind(this))
    }

  }

  clickLogic() {
    // If disable is on don't allow event if active
    if(this.$selector.data("gef-class-toggle-disable")) {
      // If the current button isn't active
      if(!this.$selector.hasClass(this.config.selectors.active)) {
        // Remove active class on all buttons for target
        $("[data-gef-class-toggle-target='" + this.toggleTarget + "']").removeClass(this.config.selectors.active)
        // Add active class to clicked button
        this.$selector.addClass(this.config.selectors.active)
        // Toggle class of target
        $(this.toggleTarget).toggleClass(this.toggleClass)
      }
    } else {
      // If target has toggle class
      if(this.toggleTarget.hasClass(this.toggleClass)) {
        // Remove active class from button
        this.$selector.removeClass(this.config.selectors.active)
        // Remove toggle class from target
        $(this.toggleTarget).removeClass(this.toggleClass)
      } else {
        // Add active class to button
        this.$selector.addClass(this.config.selectors.active)
        // Add toggle class to target
        $(this.toggleTarget).addClass(this.toggleClass)
      }
    }
    return this.$selector
  }

}


/**
* Exports the Class Toggle class as a module
* @module
*/
export default ClassToggle
