"use strict"
import * as $ from "jquery"

/**
 * Sends events to Google Analytics for precise statistics for downloaded filetypes
 *
 * @since 2.0.2
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */


class GaEventTracker {

  /**
  * Creates a new AnchorBox
  *
  * @constructor
  *
  * @param {Object}   fileTypes - the extensions and their category to be referenced by GA
  *
  */

  constructor(selector, config) {

    this.$selector = $(selector)

    this.config = {

      events: {
        downloads: {
          types : {
            "Image": ['gif', 'jpg', 'jpeg', 'png'],
            "Word": ['doc', 'docx', 'dotx', 'dot', 'dotm', 'docm', 'rtf'],
            "PDF": ['pdf'],
            "Document": ['rotx', 'pub', 'txt', 'ppt', 'pptx', 'pps', 'pptm', 'xps', 'xls', 'xlsx', 'xlsm', 'xml', 'xlsb'],
            "Video": ['flv', 'wmv', 'avi', 'mkv', 'mp4'],
            "Audio": ['mp3', 'ogg', 'wav', 'wma']
          }
        }
      }
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Run the apply download event functions
    if (this.config.events.downloads) {
      this.applyDownloadEvent()
    }
  }

  /**
  * Loops through all the different types of download and set approrpiate events
  *
  * @applyDownloadEvent
  *
  */

  applyDownloadEvent() {

    var fileTypes = this.config.events.downloads.types

    for (var fileCategory in fileTypes) {
      var extentions = fileTypes[fileCategory],
          extensionsArr = []

      for (var extentionIndex in extentions) {
        extensionsArr[extentionIndex] = "\\." + extentions[extentionIndex]
      }

      var re = new RegExp("(" + extensionsArr.join("|") + ")$", "i");

      this.$selector.filter(function() {
        return re.test($(this).attr('href'))
      }).each(function(a) {
        $(this).attr("link-category", fileCategory)
      }).bind("click", function(e) {
        ga('send', 'event', 'File download', $(this).attr("link-category"), $(this).attr("href"))
      }).bind("contextmenu", function(e) {
        ga('send', 'event', 'File download', $(this).attr("link-category"), $(this).attr("href"))
      })
    }
  }
}

export default GaEventTracker