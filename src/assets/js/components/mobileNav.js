"use strict"
import * as $ from "jquery"

class MobileNav {

  /**
   * @constructor
   * @param {string} selector - CSS selector or jQuery object of HTML element to use
   * @param {object} config   - class configuration arguments
   * @param {object} config.selectors  - CSS selectors to use in conjunction with class selector
   * @param {string} config.selectors.nav           - Main navigation CSS selector
   * @param {string} config.selectors.navButton     - Main navigation toggle CSS selector
   * @param {string} config.selectors.search        - Search form CSS selector
   * @param {string} config.selectors.searchButton  - Search button toggle CSS selector
   * @param {object} config.modifiers  - CSS class names for state changes
   * @param {string} config.modifiers.active        - Active state class
   * @param {int} config.mobileWidth   - Maximum width of window to make changes
   */
  constructor(selector, config) {

    this.config = {
      selectors: {
        nav: ".gef-nav",
        navButton: ".gef-nav__toggle",
        search: "form.gef-search__toggle-target",
        searchButton: ".gef-search__toggle"
      },
      modifiers: {
        active: "active"
      },
      mobileWidth: "960px"
    }

    // Merge default config with passed config
    $.extend(this.config, config)

    // Setting class variables
    this.$nav = $(this.config.selectors.nav)
    this.$navButton = $(this.config.selectors.navButton)
    this.$search = $(this.config.selectors.search)
    this.$searchButton = $(this.config.selectors.searchButton)
    this.$mql = window.matchMedia('screen and (max-width: ${this.config.mobileWidth})')

    // If viewing on mobile
    if(this.$mql.matches) {
      // Reset both the navigation and search so its hidden when page loads
      this.resetNav()
      this.resetSearch()
    }

    // Add event listener for portait to landscape
    // Commenting out below code as not required for now

    window.addEventListener('resize', () => {
      if(!this.$mql.matches) {
        this.activateNav()
        //this.activateSearch()
      } else {
        this.resetNav()
        //this.resetSearch()
      }
    }, true)

    // On touch of navigation toggle button...
    this.$navButton.on("click", () => {
      this.toggleNavigation()
    })

    // On touch of search toggle button...
    this.$searchButton.on("click", () => {
      this.toggleSearch()
    })

  }

  // Toggle navigation and update accessibility attributes
  toggleNavigation() {
    if(!this.$navButton.hasClass(this.config.modifiers.active)) {
      // If navigation isn't already opened..
      // Remove search
      this.resetSearch()
      // Open nav
      this.activateNav()
    } else {
      this.resetNav()
    }
    return this
  }

  // Toggle search and update accessibility attributes
  toggleSearch() {
    if(!this.$searchButton.hasClass(this.config.modifiers.active)) {
      // If search isn't already opened..
      // Close dat nav
      this.resetNav()
      // Open the search
      this.activateSearch()
    } else {
      // If search is already opened close it..
      this.resetSearch()
    }
    return this
  }

  // Navigation open
  activateNav() {
    this.$navButton.addClass(this.config.modifiers.active)
    this.$nav.attr("aria-hidden", "false").addClass(this.config.modifiers.active)
    this.$nav.find("a").attr("tabindex", "0")
    return this
  }

  // Search open
  activateSearch() {
    this.$searchButton.addClass(this.config.modifiers.active)
    this.$search.attr({"aria-hidden": "false"}).addClass(this.config.modifiers.active)
    this.$search.find("input, button").attr("tabindex", "0")
    return this
  }

  // Navigation close
  resetNav() {
    this.$navButton.removeClass(this.config.modifiers.active)
    this.$nav.attr("aria-hidden", "true").removeClass(this.config.modifiers.active)
    this.$nav.find("a").attr("tabindex", "-1")
    return this
  }

  // Search close
  resetSearch() {
    this.$searchButton.removeClass(this.config.modifiers.active)
    this.$search.attr({"aria-hidden": "true"}).removeClass(this.config.modifiers.active)
    this.$search.find("input, button").attr("tabindex", "-1")
    return this
  }

  // Static function to instatiate class as singleton
  //
  // @param {string|object} selector - CSS selector of back to top button
  // @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
  static shared(selector, config) {
    this.instance != null ? this.instance : this.instance = new MobileNav(selector, config)
    return this.instance
  }

}

/**
 * mobileNav navigation and search button toggling and accessibility
 * @module components/mobileNav
 */
export default MobileNav
