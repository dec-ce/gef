import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.REGIONSCHOOLS_SET:
            return setRegionSchools(state, action)
        default:
            return state
    }
}

function setRegionSchools(state, action) {
    const { regionschools } = action
    return regionschools
}