"use strict"
import NotificationCentre from "core/notificationCentre"
import Environment from "core/environment"
import * as $ from "jquery"


class SearchIndexChecker {

  constructor(selector, config) {

    // Default class config options
    this.config = {
      checkOnLoad: false,
      environment: "auto",
      collections: [
        "public_ce_dec_nsw_gov_au", // public education.nsw collection
        "secure_ce_intranet", // "Inside the department" collection
        "public_ce_school_website_service" // SWS collection
      ]
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }


    

    // Set up a instance level reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    // Check the page the script is being executed on as soon as the SearchIndexChecker is loaded
    if (this.config.checkOnLoad) {
      
      this.url = `${window.location.host}${window.location.pathname}`

      if (window.location.href.match("checkthispage=false")) {
        let suppliedURL = prompt("Please enter the URL of the page you would like to check")
        if (suppliedURL) {
          this.url = suppliedURL
        }
      }

      for (let collection of this.config.collections) {
        this.checkURL(this.url, collection)
      }

    }

  }

  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new SearchIndexChecker(selector, config)
  }

  checkURL(url, collection) {

    let $deferred = $.Deferred()

    $.ajax({
      method: "GET",
      url: Environment.get(this.config.environment , "onisearch") + "/search",
      dataType: "json",
      data: {
        site: collection,
        as_sitesearch: url,
        client: "doe",
        format: "json"
      },
      success: (data, textStatus, jqXHR) => {

        // Diagnostic update
        this.notificationCentre.publish("diagnosticUpdate", {
          module: "SearchIndexChecker",
          messages: [
            { text: `URL`, variable: url },
            { text: `Search server`, variable: this.config.environment },
            { text: `Collection`, variable: collection },
            { text: `Number of results`, variable: data.totalHits }
          ]
        })

        $deferred.resolve(data, url, collection)
      },
      error: (jqXHR, status, error) => {

        // Diagnostic update
        this.notificationCentre.publish("diagnosticUpdate", {
          module: "SearchIndexChecker",
          messages: [
            { text: `Error connecting to OniSearch`, variable: `¯\_(ツ)_/¯` }
          ]
        })

        $deferred.reject(jqXHR, status, error)
      }
    })

    return $deferred.promise()

  }

}


/**
 * Exports the SearchIndexChecker class as a module
 * @module
 */
export default SearchIndexChecker