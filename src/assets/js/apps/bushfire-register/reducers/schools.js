import * as actionTypes from "../constants/actionTypes"

const initialState = null

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SCHOOLS_SET:
            return setSchools(state, action)
        default:
            return state
    }
}

function setSchools(state, action) {
    const { schools } = action
    return schools
}