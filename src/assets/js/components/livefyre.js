"use strict"
import * as $ from "jquery"
//import * as Base64 from "js-base64"

(function($) {
  let maxLength = 8
  let uatLink = "https://nsweducation-uat.bootstrap.fyre.co/bs3/v3.1/nsweducation-uat.fyre.co/385880/"
  let prodLink = "https://nsweducation.bootstrap.fyre.co/bs3/v3.1/nsweducation.fyre.co/385899/"
  //  console.log(Base64)
    //let b64 = $.base64(encode)("designer-app-1540445549175")
    //console.log("b64", b64)

// work out which URL to loader
  let urlToFetch = prodLink
  if ($(".livefyre-mosaic").attr("livefyreType") == "uat") {
    urlToFetch = uatLink
  }
  if ($(".livefyre-mosaic").attr("livefyreID")) {
    urlToFetch += $(".livefyre-mosaic").attr("livefyreID") + "/init"
  } else {
    console.error("no livefyre value found")
  }
    fetchLiveFyre(urlToFetch) // initial fetch
  $('#submitForm').click(function() {
    $(".livefyre-mosaic").html("")
    let typeOfPage = $("input[name=q_type]:checked").val()
    let fetchUrl = uatLink
    if (typeOfPage == "prod") {
      fetchUrl = prodLink
    }
    fetchUrl += $("#inputCode").val() + "/init"
    fetchLiveFyre(fetchUrl)
  })


/** Function fetchLiveFyre(url)
* insert the url required
* initiates ajax call and processes
**/

function fetchLiveFyre(url) {
  let response2 = $.ajax({
      url: url
  }).done(function(data) {
    //  console.log("response 2", data)
      processReturn(data)
  }).fail(function(data){
      console.log("error response 2", data)
  })
}
/** function processReturn
* Expects data from the ajax call, to include a headDocument.content return, which contains all the information about the item.

**/

function processReturn(data) {
  if (!data) {
    console.error("no data");
  } else {
    let headData = data.headDocument
    //console.log(headData)
    if (headData) {
      let contentReturned = headData.content
      console.log(contentReturned)
      let itemsToShow = maxLength > contentReturned.length ? contentReturned.length : maxLength
      console.log(itemsToShow)
      let liveFyreHTML = ""
      for (var i=0; i<itemsToShow; i++) {
        liveFyreHTML = processItem(contentReturned[i])? liveFyreHTML + processItem(contentReturned[i]) : liveFyreHTML
      }
      $(".livefyre-mosaic").html(liveFyreHTML)
    }
  }
}


/**
  function processItem
  Takes the individual items and processes them according to type
  Structure is:
  - collectionId
  - collection - contains all the information about the processItem
  - event
  - source - where it come from.  19 is Twitter, 20 is Instagram.  Not sure what other numbers are at the moment.  0 is manually uploaded.
  - type - no idea what this is either.  Their documnetation sucks
  - vis - again, who knows.
  - the actual details live unnder the content.attachments [0] though
**/
function processItem(item) {
  let htmlStructure = "<div class=\"livefyre-item uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-1 \">"
  htmlStructure +=   "<h2 class=\"show-on-sr\">Social Media component from livefyre</h2>"
  htmlStructure +=   "<div class=\"livefyre-image-location\"><a href=\"SHARE\">THUMBNAIL<\/a><\/div>"
  htmlStructure +=   "<div class=\"livefyre-branding\"><\/div>"
  htmlStructure +=   "<div class=\"livefyre-wording\">"
  htmlStructure +=   "<p>INTRODUCTION<\/p>"
  htmlStructure +=   "<\/div>"
  htmlStructure +=   "<div class=\"livefyre-footer uk-width-1-1\">"
  htmlStructure +=   " <div class=\"livefyre-date-and-type uk-width-1-2\">"
  htmlStructure +=   "<div class=\"livefyre-type MEDIATYPE\"><\/div>"
  htmlStructure +=   "<div class=\"livefyre-date\">"
  htmlStructure +=   "<datetime>DATESENT<\/datetime>"
  htmlStructure +=   "<\/div>"
  htmlStructure +=   "<\/div>"
  htmlStructure +=   "<div class=\"livefyre-share uk-width-1-3\"><a href=\"SHARE\">Share<\/a><\/div>"
  htmlStructure +=   "<\/div>"
  htmlStructure +=   "<\/div>"
  let itemContent = item.content
  console.log(itemContent)
  if (itemContent) {
    let itemAttachment = itemContent.attachments;
    //console.log(itemAttachment)
    if (itemAttachment) {
      let itemDetails = itemAttachment[0]
      // get the base information for this
      let baseCode = htmlStructure
      baseCode = baseCode.replace("MEDIATYPE", "livefyre-" + itemDetails.provider_name)
      baseCode = baseCode.replace("THUMBNAIL", "<img src=\"" + itemDetails.thumbnail_url + "\" />")
      if (itemDetails.title.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "").length > 100) {
        baseCode = baseCode.replace("INTRODUCTION", itemDetails.title.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "").substring(0, 97) + "...")
      } else {
        baseCode = baseCode.replace("INTRODUCTION", itemDetails.title.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "").substring(0, 100))
      }
      let thisDate = new Date(itemContent.createdAt*1000)
      //console.log(thisDate)
      let formattedDate = thisDate.getDate() + "/" + (thisDate.getMonth() + 1) + "/" + thisDate.getFullYear()
      baseCode = baseCode.replace("DATESENT", formattedDate)
      baseCode = baseCode.replace(/SHARE/g, itemDetails.link)
      return baseCode
    } else {
      return ""
    }
  } else {
    return ""
  }

}

}) (jQuery)
